/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package periodictask;

import java.sql.Connection;
import java.sql.Date;
import java.sql.Statement;
import periodictask.utilitaire.Utilitaire;

/**
 *
 * @author tahina
 */
public class ReqRecu extends HistoriqueSync {

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }
    java.sql.Date daty;
    String heure;
    public ReqRecu(String id, String action, String tableconcerne, String requete,String etat) {
        super(id,action,tableconcerne,requete,etat);
        //this.setNomTable("ReqRecu");
        this.setDaty(Utilitaire.dateDuJourSql());
        this.setHeure(Utilitaire.heureCouranteHMS());
    }
    public ReqRecu(String action, String tableconcerne, String requete,String etat) {
        super("",action,tableconcerne,requete,etat);
        //this.setNomTable("ReqRecu");
        this.setDaty(Utilitaire.dateDuJourSql());
        this.setHeure(Utilitaire.heureCouranteHMS());
    }
    public int execute(Connection c) throws Exception
    {
        if(this.getEtat()==11)return 0;
        Statement st=null;
        int retour=0;
        try
        {
            String req=this.getRequeteEscaped();
            st=c.createStatement();
            retour= st.executeUpdate(req);
        }
        catch(Exception e)
        {            
            throw e;  
        }
        finally{
            if(st!=null)st.close();
            
        }
        return retour;
    }
    
    
}
