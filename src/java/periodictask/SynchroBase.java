/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package periodictask;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.TimerTask;
import java.util.ArrayList;
import java.util.TimeZone;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.*;
import java.sql.Statement;

import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.security.MessageDigest;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import periodictask.bean.CGenUtil;
import periodictask.ui.Monitor;
import periodictask.utilitaire.UtilDB;
import periodictask.utils.Constant;
import periodictask.utils.CustomTime;
import periodictask.utilitaire.Utilitaire;

/**
 *
 * @author Andy RANDRIANIRINA
 */
public class SynchroBase extends TimerTask {

    private static long time = 0;
    private static int LIMIT_REQUEST_PER_SCHEDULE = 1000;
    private static String[] SPECIFIC_TABLES = new String[]{};

    public static String ipsource,
            username,
            password;

    private static List<Server_Broadcast> _serv;

    public static Connection GetConn() throws Exception {
        return SynchroBase.GetConn(ipsource, username, password);
    }

    public static void execute() throws Exception {
        Connection con = null;
        ArrayList<Server_Broadcast> servers = null;
        PreparedStatement stGroupe=null;
        PreparedStatement statmentDist=null;
        PreparedStatement statmentLoc=null;
        try {
            LogSynchro.println("[INFO]: Début de la synchronisation");
            //connection = SynchroBase.GetConn();

            con = (new UtilDB()).GetConn();
            con.setAutoCommit(false);

            // Execution local
            LogSynchro.println("[INFO]: Reception requetes");
            // maka anizay requete_recu_groupe etat = 1 de manao insert ao amin'ny historique_sync
            new RequeteRecuGroupe().traitementREqueteRecu(con);
            ArrayList<HistoriqueSync> requestsToExecute = getRequetteAExecuter(con);
            LogSynchro.println("[INFO]: Reception des requetes recus");
            executeRequeteRecu(con, requestsToExecute);
            con.commit();
            LogSynchro.println("INFO: Execution des requetes recus ");
            //Alaina ny liste serveur cible
            servers = SynchroBase.getServers(con);
            _serv = servers;
			 LogSynchro.println("servers tonga"+servers.size());
            // boucle liste serveurs
            stGroupe=RequeteAenvoyerGroupe.getPrepared(con);
            statmentLoc=RequeteExecute.preparer(con);
            for (Server_Broadcast server : servers) {
                if (server != null) {
                    Connection connection = null;
                    try {
                        // connection a une serveur
                        connection = server.GetConnect();
                        //connection.setAutoCommit(false);
                        LogSynchro.println("INFO: Connection established [" + server.getNom() + "]");
                        LogSynchro.println("INFO: Enregistrement des operations! Veuillez ne plus interrompre le programme");
                        if (connection != null) {
                            NamedServerConnection connectionRow = new NamedServerConnection(server);
                            
                            connectionRow.setConnection(connection);
                            //result.add(nc);
                            ArrayList<RequeteAEnvoyer> requests = null;

                            // step one: 
                            LogSynchro.println("INFO[" + connectionRow.getNom() + "]: recuperation des requetes a envoyer ... ");
                            //requests = getHistoriqueSync(connectionRow);
                            requests = getRequetteAEnvoyer(con, server);
                            //RequeteAenvoyerGroupe requeteAenvoyerGroupe = new RequeteAenvoyerGroupe(requests);
                            //envoyeRequeteGroupe(connectionRow, requeteAenvoyerGroupe.grouperRequeteAenvoyer(con), con);
                            AdminReqGroupe listeGroupe=new AdminReqGroupe(requests);
                            statmentDist=RequeteRecuGroupe.preparer(connectionRow.getConnection());
                            for(RequeteAenvoyerGroupe requeteAenvoyerGroupe:listeGroupe.getListeGroupe())
                            {
                                requeteAenvoyerGroupe.inserer(stGroupe);
                                envoyeRequeteGroupe(connectionRow,requeteAenvoyerGroupe , statmentDist,statmentLoc);
                            }
                            LogSynchro.println("INFO[" + connectionRow.getNom() + "]: Enregistrement OK!");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    } finally {
						if(statmentDist != null){
							statmentDist.close();
						}
                        if (connection != null) {
                            connection.close();
                        }
                    }
                }
            }
            con.commit();
            LogSynchro.println("INFO: Fin du programme de synchronisation! SUCCES! En attente de la prochaine synchronisation");

        } catch (Exception e) {
            e.printStackTrace();
            LogSynchro.println(e.getMessage());
            con.rollback();
            throw e;
        } finally {
            if(stGroupe!=null)stGroupe.close();
            if(statmentLoc!=null)statmentLoc.close();
            if (con != null) {
                con.close();
            }
            LogSynchro.println("INFO: Synchronisation terminee");
            CustomTime _time = new CustomTime().fromMillisecond(time);
            String tt = String.format("%02dd:%02dh:%02dm:%02ds", _time.getDays(), _time.getHours(), _time.getMinutes(), _time.getSeconds());
            LogSynchro.println("INFO: Synchronisation suivante dans [" + tt + "]");
            LogSynchro.println("----------------------------------- " + new Date().toString());
        }
    }

    /*
        Pour executer une requête dans la connection specifiee 
        Seulement pour les requêtes depuis l'Historique
     */
    public static boolean execute(Connection connection, String request) throws Exception {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            String refactor = Utilitaire.refactorNumbers(request.replace("\"", "'").replace(";", " "));
            int t = statement.executeUpdate(refactor);
            return t > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            //Au cas où la ligne est deja dans la base a cause d'une coupure
            if (e.getMessage().toLowerCase().contains("unique constraint") || e.getMessage().toLowerCase().contains("contrainte unique")) {
                return false;
            } else {
                //LogSynchro.println("ERROR: " + e.getMessage());
                return false;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    public static void setTime(long time) {
        SynchroBase.time = time;
    }

    /*
        Connecter chaque serveur a la synchronisation principale
     */
    public static ArrayList<NamedServerConnection> connect(ArrayList<Server_Broadcast> servers) throws Exception {
        LogSynchro.println("INFO: Connexion vers les serveurs");
        ArrayList<NamedServerConnection> result = new ArrayList<NamedServerConnection>();
        Connection connection = null;
        for (Server_Broadcast server : servers) {
            if (server != null) {
                LogSynchro.println("INFO: Serveur " + server.getAdresse_ip() + "/" + server.getNom());
                try {
                    connection = server.GetConn();
                    if (connection != null) {
                        NamedServerConnection nc = new NamedServerConnection(server);
                        nc.setConnection(connection);
                        result.add(nc);
                    }
                } catch (Exception e) {
                    continue;
                }
            }
        }
        return result;
    }

    public static void executeRequeteRecu(Connection connection, ArrayList<HistoriqueSync> request) throws Exception {
        Statement statement = null;
        try {
            if (request != null && request.size() > 0) {
                LogSynchro.println("INFO[]: X: " + request.size() + " requetes a execute");
                LogSynchro.println("INFO[]: Execution des requetes depuis la source...");
                //SynchroBase.insertMainRequest(connectionRow,connection,requests);
                statement = connection.createStatement();
                int count = 1;

                for (HistoriqueSync requests : request) {
                    //execution
                    String query = requests.getRequete().replace("''", "'");
                    LogSynchro.println("INFO[]: X: " + query);
                    //statement = connection.prepareStatement(query);
                    int valiny = 0;
                    try {
                        valiny = statement.executeUpdate(query);
                        //valiny = statement.executeUpdate();
                    } catch (SQLIntegrityConstraintViolationException exc) {
                        requests.setEtat(11);
                        String req = " UPDATE HISTORIQUE_SYNC set etat = 11 where id = '" + requests.getId() + "'";
                        statement.executeUpdate(req);
                        exc.printStackTrace();
                        continue;
                    } catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    }
                    
                    if (valiny > 0||(valiny==0&&(requests.getAction().compareToIgnoreCase("update")==0||requests.getAction().compareToIgnoreCase("delete")==0))) {
                        // maj etat requette
                        requests.setEtat(11);
                        //LogSynchro.println("UPDATE []: X: " + query);

                        String req = " UPDATE HISTORIQUE_SYNC set etat = 11 where id = '" + requests.getId() + "'";
                        //statement.executeUpdate(req);
                       // statement = connection.prepareStatement(req);
                        statement.executeUpdate(req);
                        //requests.updateToTable(connection);
                        //connection.commit();
                    }
                    count++;
                    LogSynchro.println("INFO[]: X: " + count + "/" + request.size() + " requetes execute");
                }
            } else {
                LogSynchro.println("INFO[]: Aucune requete a execute pour ce serveur.");
            }
        } catch (Exception exc) {
            exc.printStackTrace();
            connection.rollback();
            //throw exc;
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    public static void envoyeRequete(NamedServerConnection connectionRow, ArrayList<RequeteAEnvoyer> request, Connection connectionLocal) throws Exception {
        PreparedStatement statement = null;
        Statement statement2 = null;
        try {
            statement2 = connectionLocal.createStatement();
            if (request != null && request.size() > 0) {
                LogSynchro.println("INFO[" + connectionRow.getNom() + "]: X: " + request.size() + " requetes a envoyer");
                LogSynchro.println("INFO[" + connectionRow.getNom() + "]: Execution des requetes depuis la source...");
                int indice = 1;

                for (RequeteAEnvoyer requests : request) {

                    String req = " INSERT INTO HISTORIQUE_SYNC (ID,ACTION,TABLECONCERNE,REQUETE,DATY,HEURE,SERVEUR,ETAT) values ('HS'||GETSEQHISTORIQUESYNC,?,?,?,?,?,?,1)";

                    statement = connectionRow.getConnection().prepareStatement(req);
                    statement.setString(1, requests.getAction());
                    statement.setString(2, requests.getTableconcerne());
                    statement.setString(3, requests.getRequete());
                    statement.setDate(4, requests.getDaty());
                    statement.setString(5, requests.getHeure());
                    statement.setString(6, connectionRow.getNom());
                    int valiny = statement.executeUpdate();
                    LogSynchro.println("INFO[" + connectionRow.getNom() + "]: X: " + indice + "/" + request.size() + " ");
                    String req2 = " INSERT INTO REQUETE_EXECUTE (ID, IDREQUETE, DATY, HEURE, SERVEUR) values ";
                    req2 += "('REXC00'||getSeqRequeteExecute,'" + requests.getId() + "','" + Utilitaire.dateDuJour() + "','" + Utilitaire.heureCouranteHMS() + "','" + connectionRow.getId() + "')";
                    if (valiny > 0) {
                        statement2.executeUpdate(req2);
                    }
                    indice++;
                }
            } else {
                LogSynchro.println("INFO[" + connectionRow.getNom() + "]: Aucune requete a envoyer pour ce serveur.");
            }
        } catch (Exception e) {

            e.printStackTrace();
            LogSynchro.println("INFO[" + connectionRow.getNom() + "]: " + e.getMessage());
            throw e;
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (statement2 != null) {
                statement2.close();
            }
        }
    }

    /*
        Obtenir la liste des requetes a synchroniser pour la connection specifiee (Connexion)
     */
    public static ArrayList<HistoriqueSync> getRequetteAExecuter(Connection connection) throws Exception {
        Statement stmtServ = null;
        try {
            /*HistoriqueSync hsc = new HistoriqueSync();
            hsc.setNomTable("HISTORIQUE_SYNC");
            HistoriqueSync[] listRequete = (HistoriqueSync[]) CGenUtil.rechercher(hsc, null, null, connection, " and ETAT = 1 ORDER BY id asc");
            return listRequete;*/

            String req = " select * from HISTORIQUE_SYNC where etat = 1 order by daty asc, heure asc, id asc";
            stmtServ = connection.createStatement();
            ResultSet rs = stmtServ.executeQuery(req);

            return HistoriqueSync.getAll(rs);
        } catch (Exception e) {
            e.printStackTrace();
            connection.rollback();
            throw new Exception(e.getMessage());
        } finally {
            if (stmtServ != null) {
                stmtServ.close();
            }
        }
    }

    public static ArrayList<RequeteAEnvoyer> getRequetteAEnvoyer(Connection connection, Server_Broadcast server) throws Exception {
        LogSynchro.println("niditra ******************** getRequetteAEnvoyer");
        Statement stmtServ = null;
        try {
            /*
            RequeteAEnvoyer hsc = new RequeteAEnvoyer();
            hsc.setNomTable("requete_a_envoyer_serveur");
            RequeteAEnvoyer[] listRequete = (RequeteAEnvoyer[]) CGenUtil.rechercher(hsc, null, null, connection, "  and SERVEUR is null or SERVEUR != '" + server.getId() + "' ORDER BY id asc");
             */
            //String getServ = "SELECT * FROM(SELECT * FROM requete_a_envoyer_serveur where id not in (select id from REQUETE_EXECUTE_SERVEUR where SERVEUR = '" + server.getId() + "') ORDER BY daty asc,heure asc,id asc) where rownum <= 1000";
            String getServ = "SELECT * FROM requete_a_envoyer_serveur where id not in (select id from REQUETE_EXECUTE_SERVEUR where SERVEUR = '" + server.getId() + "') ORDER BY daty asc,heure asc,id asc ";
            
            stmtServ = connection.createStatement();
            LogSynchro.println(getServ);
            ResultSet rs = stmtServ.executeQuery(getServ);
            return RequeteAEnvoyer.getAll(rs);

            //return listRequete;
        } catch (Exception e) {
            e.printStackTrace();
            connection.rollback();
            throw new Exception(e.getMessage());
        } finally {
            if (stmtServ != null) {
                stmtServ.close();
            }
        }
    }

    /*
        Obtenir la liste des requetes a synchroniser pour la connection specifiee (Connexion)
     */
    public static ArrayList<HistoriqueSync> getHistoriqueSync(NamedServerConnection connection) throws Exception {
        LogSynchro.println("INFO[" + connection.getNom() + "]: Setting auto_commit to <false>");
        //connection.getConnection().setAutoCommit(false);
        LogSynchro.println("INFO[" + connection.getNom() + "]: Verification historique des donnees non synchronisees");

        String sql_count = "SELECT count(*) as count from HISTORIQUE_SYNC WHERE ETAT<11 ORDER BY id asc";
        LogSynchro.println(sql_count);
        Statement cc = connection.getConnection().createStatement();
        ResultSet rsc = cc.executeQuery(sql_count);
        int c = 1;
        while (rsc.next()) {
            //LogSynchro.println("INFO: [" + rsc.getString("count") + "] requetes");
            c = rsc.getInt("count");
        }//*/
        if (c <= 1) {
            return new ArrayList<HistoriqueSync>();
        }
        //String getHisto = "SELECT * FROM HISTORIQUE_SYNC WHERE ETAT <11 and ROWNUM<="+LIMIT_REQUEST_PER_SCHEDULE+" ORDER BY id asc";
        String getHisto = "SELECT * FROM HISTORIQUE_SYNC WHERE ETAT<11 ORDER BY id asc";
        LogSynchro.println(getHisto);
        Statement stmtHisto = connection.getConnection().createStatement();
        stmtHisto.setFetchSize(512);
        ResultSet rs = stmtHisto.executeQuery(getHisto);//*/
        return HistoriqueSync.getAll(rs);
    }

    /*
        Obtenir la liste des serveurs a synchroniser
     */
    public static ArrayList<Server_Broadcast> getServers(Connection connection) throws Exception {
        Statement stmtServ = connection.createStatement();
        ArrayList<Server_Broadcast> ret = null;
        try{
            LogSynchro.println("INFO: Recuperation de la liste des serveurs a synchroniser");
            String getServ = "SELECT * FROM Server_Broadcast";
            LogSynchro.println(getServ);
            ResultSet rs = stmtServ.executeQuery(getServ);
            ret =  Server_Broadcast.getAll(rs);
        } catch (Exception e) {
            if (connection != null) {
                connection.rollback();
            }
        } finally {
            if (stmtServ != null) {
                stmtServ.close();
            }
        }
        return ret;
    }

    /*
        Insertion des requetes dans le serveur principal
        ne pas mettre a jour en cas d'erreur
     */
    public static void insertMainRequest(NamedServerConnection source, Connection destination, ArrayList<HistoriqueSync> requests) throws Exception {
        LogSynchro.println("INFO[" + source.getNom() + "]: Execution des requetes depuis la source...");
        try {
            int count = 0;
            for (HistoriqueSync request : requests) {
                request.setTablename("HIST_SYNC_BROADCAST");
                if (request.insert(destination)) {
                    request.setTablename("HISTORIQUE_SYNC");
                    request.updateEtat(source.getConnection());
                    LogSynchro.print("INFO: PROGRESS[" + count + "/" + requests.size() + "]", count == requests.size());
                    //LogSynchro.printProgress(System.currentTimeMillis(), requests.size(), count);
                    count++;
                }
            }
        } catch (Exception e) {
            LogSynchro.println("ERROR: Une erreur a ete lancee depuis l'insertion dans HIST_SYNC_BROADCAST");
            throw e;
        }
    }

    /*
        Insertion des requetes dans le serveur principal
        ne pas mettre a jour en cas d'erreur
     */
    public static void exportToFile(FileOutputStream out, NamedServerConnection source, ArrayList<HistoriqueSync> requests) throws Exception {
        LogSynchro.println("INFO[" + source.getNom() + "]: Execution des requetes depuis la source...");
        try {
            int count = 1;
            String s = "";
            for (HistoriqueSync request : requests) {
                s += request.getRequete() + Constant.DELIMITER;
            }
            byte[] data = Utilitaire.encrypt(s);
            out.write(data, 0, data.length);
            for (HistoriqueSync request : requests) {
                request.setTablename("HISTORIQUE_SYNC");
                //request.updateEtat(source.getConnection());
                LogSynchro.print("INFO: PROGRESS[" + count + "/" + requests.size() + "]", count == requests.size());
                count++;
            }
        } catch (Exception e) {
            LogSynchro.println("ERROR: Une erreur a ete lancee depuis l'insertion dans HIST_SYNC_BROADCAST");
            throw e;
        }
    }

    /*
        Metier Synchronisation
     */
    public static void stepone(Connection connection, ArrayList<NamedServerConnection> connectionRows) throws Exception {
        LogSynchro.println("INFO: Starting step 1");
        ArrayList<HistoriqueSync> requests = null;
        //Pour chaque serveur cible                    
        if (connectionRows != null && !connectionRows.isEmpty()) {
            for (NamedServerConnection connectionRow : connectionRows) {

                LogSynchro.println("INFO[" + connectionRow.getNom() + "]: stepone -- avant getHistoriqueSync");
                requests = getHistoriqueSync(connectionRow);
                if (requests != null && !requests.isEmpty()) {
                    LogSynchro.println("INFO[" + connectionRow.getNom() + "]: X: " + requests.size() + " requetes a executer");
                    SynchroBase.insertMainRequest(connectionRow, connection, requests);
                } else {
                    LogSynchro.println("INFO[" + connectionRow.getNom() + "]: Aucune requete a executer pour ce serveur.");
                }
            }
        } else {
            LogSynchro.println("WARNING: Aucune connexion etablie! Veuillez verifier la liste des serveurs");
        }
        LogSynchro.println("INFO: Ending step 1");
    }

    public static void steptwo(Connection connection, ArrayList<NamedServerConnection> connectionRows) throws Exception {
        LogSynchro.println("INFO: Starting step 2");
        ResultSet rsA = null, rsB = null;
        Statement statementA = null, statementB = null;
        ArrayList<HistoriqueSync> toSkip = null, toInsert = null;

        //Maka an'izy rehetra
        String queryA = "";
        String queryB = "";

        try {
            if (connectionRows != null && !connectionRows.isEmpty()) {
                for (NamedServerConnection connectionRow : connectionRows) {
                    LogSynchro.println("INFO[" + connectionRow.getNom() + "]: Donnees a ignorer pendant la synchronisation");

                    String sql_count = "SELECT count(*) as count FROM HISTORIQUE_SYNC";
                    LogSynchro.println(sql_count);
                    Statement cc = connectionRow.getConnection().createStatement();
                    ResultSet rsc = cc.executeQuery(sql_count);
                    int c = 1;
                    while (rsc.next()) {
                        LogSynchro.println("INFO: [" + rsc.getInt("count") + "] requetes");
                        c = rsc.getInt("count");
                    }//*/

                    queryA = "SELECT * FROM HISTORIQUE_SYNC";
                    statementA = connectionRow.getConnection().createStatement();
                    statementA.setFetchSize(512);
                    LogSynchro.println(queryA);
                    rsA = statementA.executeQuery(queryA);
                    toSkip = HistoriqueSync.getAll(rsA);

                    insertBatchId(connection, toSkip);

                    int count_b = 0;
                    String sql_count_b = "";
                    if (toSkip != null && toSkip.size() > 0) {
                        LogSynchro.println("INFO[" + connectionRow.getNom() + "]: Request -- " + toSkip.size() + " donnees a ignorer pour ce serveur");
                        queryB = "SELECT * FROM HIST_SYNC_BROADCAST WHERE ID NOT IN (";
                        queryB += " SELECT id FROM HIST_SYNC_TEMP ";
                        queryB += ")";

                        //manisa requêtes
                        sql_count_b = "SELECT count(*) as count FROM HIST_SYNC_BROADCAST where ID NOT IN ( SELECT id FROM HIST_SYNC_TEMP )";
                    } else {
                        queryB = "SELECT * FROM HIST_SYNC_BROADCAST";
                        LogSynchro.println("INFO[" + connectionRow.getNom() + "]: Aucune requete à ignorer");

                        //manisa requêtes
                        sql_count_b = "SELECT count(*) as count FROM HIST_SYNC_BROADCAST";
                    }

                    LogSynchro.println(sql_count_b);
                    Statement cc_b = connectionRow.getConnection().createStatement();
                    ResultSet rsc_b = cc_b.executeQuery(sql_count_b);
                    while (rsc.next()) {
                        LogSynchro.println("INFO: [" + rsc_b.getInt("count") + "] requetes");
                        count_b = rsc.getInt("count");
                    }//*/

                    statementB = connection.createStatement();
                    statementB.setFetchSize(512);
                    LogSynchro.println("INFO[" + connectionRow.getNom() + "]: Request -- " + queryB);
                    rsB = statementB.executeQuery(queryB);

                    toInsert = HistoriqueSync.getAll(rsB);
                    if (toInsert != null && toInsert.size() > 0) {
                        LogSynchro.println("INFO[" + connectionRow.getNom() + "]: Request -- " + toInsert.size() + " requetes à inserer");
                        int done = 0,
                                failed = 0;
                        for (HistoriqueSync insert : toInsert) {
                            insert.setEtat(21);
                            insert.setTablename("HISTORIQUE_SYNC");
                            //LogSynchro.println(insert.getRequete());
                            if (SynchroBase.execute(connectionRow.getConnection(), insert.getRequete()) == true) {
                                LogSynchro.println("NAVERINA NAMPIDIRINA");
                                done++;
                                insert.insert(connectionRow.getConnection());
                            } else {
                                failed++;
                            }
                            int percent = Math.round(((done + failed) / toInsert.size()) * 100);
                            LogSynchro.print(String.format("INFO: FAILED[%d] - DONE[%d] - TOTAL[%d] :: %d%%", done, failed, toInsert.size(), percent), done + failed == toInsert.size());
                        }
                    } else {
                        LogSynchro.println("INFO[" + connectionRow.getNom() + "]: Aucun résultat a insérer");
                    }
                    System.out.println();
                    if (rsA != null) {
                        rsA.close();
                    }
                    if (rsB != null) {
                        rsB.close();
                    }
                    if (statementA != null) {
                        statementA.close();
                    }
                    if (statementB != null) {
                        statementB.close();
                    }
                }
            }
        } catch (Exception e) {
            LogSynchro.println("ERROR" + e.getMessage());
            throw e;
        } finally {
            LogSynchro.println("INFO: Fermeture des Statements");
            if (rsA != null) {
                rsA.close();
            }
            if (rsB != null) {
                rsB.close();
            }
            if (statementA != null) {
                statementA.close();
            }
            if (statementB != null) {
                statementB.close();
            }
        }
        LogSynchro.println("INFO: Ending step 2");
    }

    public static void insertBatchId(Connection c, ArrayList<HistoriqueSync> toSkip) throws Exception {
        PreparedStatement st = null;
        try {
            execute(c, "DELETE FROM HIST_SYNC_TEMP");
            String query = "INSERT INTO HIST_SYNC_TEMP VALUES(?)";
            st = c.prepareStatement(query);
            for (HistoriqueSync row : toSkip) {
                st.setString(1, row.getId());
                st.addBatch();
            }
            st.executeBatch();
        } catch (Exception e) {
            throw e;
        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    /*
        Gestion de la synchronisation periodique
     */
    @Override
    public void run() {
        LogSynchro.println("Lancement de la synchronisation suivante");
        try {
            if (!Synchronisation.IS_RUNNING) {
                Synchronisation.IS_RUNNING = true;
                SynchroBase.execute();
                Synchronisation.IS_RUNNING = false;
            } else {
                LogSynchro.println("WARNING: Synchronisation deja en cours! Lancement annulee... Veuillez attendre la fin de la derniere synchronisation!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection GetConn(String ip, String user, String mdp) throws Exception {
        LogSynchro.println("INFO: Tentative de connexion");
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(mdp.getBytes());
        String encryptedString = new String(messageDigest.digest());
        //LogSynchro.println("INFO: ip remote server: "+ip);
        LogSynchro.println("INFO: [" + ip + "/User = " + user + "/Password = ***********]");
        Connection conn = null;
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Baghdad");
        TimeZone.setDefault(timeZone);
        try {
            Context jndiContext = new InitialContext();
            DataSource ds = (DataSource) jndiContext.lookup("java:jboss/datasources/CnapsDS");
            conn = ds.getConnection();
            //conn.setAutoCommit(false);
            if (conn != null) {
                LogSynchro.println("INFO: Connected\n");
            }
            return conn;
        } catch (Exception ne) {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                conn = DriverManager.getConnection("jdbc:oracle:thin:@" + ip, user, mdp);
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
                throw ex;
            } catch (SQLException ex) {
                LogSynchro.println("ERROR: UtilDB Erreur Connexion : ".concat(String.valueOf(String.valueOf(ex.getMessage()))));
                throw ex;
            }
        }
        return conn;
    }


    public static Connection GetConn(String ip, String user, String mdp,String driver,String url) throws Exception {
        LogSynchro.println("INFO: Tentative de connexion");
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(mdp.getBytes());
        String encryptedString = new String(messageDigest.digest());
        //LogSynchro.println("INFO: ip remote server: "+ip);
        LogSynchro.println("INFO: [" + ip + "/User = " + user + "/Password = ***********]");
        Connection conn = null;
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Baghdad");
        TimeZone.setDefault(timeZone);
        try {
            Context jndiContext = new InitialContext();
            DataSource ds = (DataSource) jndiContext.lookup("java:jboss/datasources/CnapsDS");
            conn = ds.getConnection();
            //conn.setAutoCommit(false);
            if (conn != null) {
                LogSynchro.println("INFO: Connected\n");
            }
            return conn;
        } catch (Exception ne) {
            try {
                Class.forName(driver);
                
                conn = DriverManager.getConnection(url, user, mdp);
                LogSynchro.println("INFO: [" + url + "/User = " + user + "/Password = ***********]");
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
                throw ex;
            } catch (SQLException ex) {
                LogSynchro.println("ERROR: UtilDB Erreur Connexion : ".concat(String.valueOf(String.valueOf(ex.getMessage()))));
                throw ex;
            }
        }
        return conn;
    }

    public static void envoyeRequeteGroupe(NamedServerConnection connectionRow, RequeteAenvoyerGroupe request,PreparedStatement statement, PreparedStatement statementLocal ) throws Exception {
        //LogSynchro.println("niditra *********************** envoyeRequeteGroupe");
        

        try {
			LogSynchro.println("request === "+request);
            if (request != null) {
                
                //statement = connectionRow.getConnection().prepareStatement(req);
//            statement.setString(1, "'RRG'||getseqrequete_recu_groupe");
                statement.setString(1, request.getAction());
                statement.setString(2, request.getTableconcerne());
                statement.setString(3, request.getRequete());
                statement.setString(4, request.getDatyg());
                statement.setString(5, request.getHeure());
                statement.setString(6, request.getServeur());
                statement.setInt(7, 1);
                statement.setString(8, request.getIdrequeteAenvoyer());
                int valiny = statement.executeUpdate();
                
                String[] tIdRequete = Utilitaire.split(request.getIdrequeteAenvoyer(), ";;");
                String[] tDaty = Utilitaire.split(request.getDatyg(), ";;");
                String[] tHeure = Utilitaire.split(request.getHeure(), ";;");
                String[] tServeur = Utilitaire.split(request.getServeur(), ";;");
					//LogSynchro.println("ETO *********************** Valiny "+valiny);
                if (valiny > 0) {
                    for (int i = 0; i < tIdRequete.length; i++) {
                        //,,
                        statementLocal.setString(1, tIdRequete[i]);
                        statementLocal.setString(2, Utilitaire.formatterDaty(tDaty[i]));
                        statementLocal.setString(3, tHeure[i]);
                        statementLocal.setString(4, connectionRow.getId());
                        statementLocal.executeUpdate();

                    }
                }
            }
            //LogSynchro.println("nivoka *********************** envoyeRequeteGroupe");

        } catch (Exception e) {
            statement.getConnection().rollback();
            if (statementLocal  != null) {
                statementLocal.getConnection().rollback();
            }
            e.printStackTrace();
            LogSynchro.println("INFO[" + connectionRow.getNom() + "]: " + e.getMessage());
            //throw e;
        } 
    }

    public static void envoyeRequeteGroupe(NamedServerConnection connectionRow, RequeteAenvoyerGroupe request, Connection connectionLocal) throws Exception {
        //LogSynchro.println("niditra *********************** envoyeRequeteGroupe");
        PreparedStatement statement = null;
        PreparedStatement statementLocal = null;

        try {
			LogSynchro.println("request === "+request);
            if (request != null) {
                
                String req = " INSERT INTO requete_recu_groupe (ID,ACTION,TABLECONCERNE,REQUETE,DATYG,HEURE,SERVEUR,ETAT,IDREQUETEAENVOYER) values ('RRG'||getseqrequete_recu_groupe,?,?,?,?,?,?,?,?)";
                statement = connectionRow.getConnection().prepareStatement(req);
//            statement.setString(1, "'RRG'||getseqrequete_recu_groupe");
                statement.setString(1, request.getAction());
                statement.setString(2, request.getTableconcerne());
                statement.setString(3, request.getRequete().replace("'", "''"));
                statement.setString(4, request.getDatyg());
                statement.setString(5, request.getHeure());
                statement.setString(6, request.getServeur());
                statement.setInt(7, 1);
                statement.setString(8, request.getIdrequeteAenvoyer());
                int valiny = statement.executeUpdate();
                
                String[] tIdRequete = Utilitaire.split(request.getIdrequeteAenvoyer(), ";;");
                String[] tDaty = Utilitaire.split(request.getDatyg(), ";;");
                String[] tHeure = Utilitaire.split(request.getHeure(), ";;");
                String[] tServeur = Utilitaire.split(request.getServeur(), ";;");
					//LogSynchro.println("ETO *********************** Valiny "+valiny);
                if (valiny > 0) {
                    String req2 = " INSERT INTO REQUETE_EXECUTE (ID, IDREQUETE, DATY, HEURE, SERVEUR) values ";
                        req2 += "('REXC00'||getSeqRequeteExecute,?,?,?,?)";
                    statementLocal = connectionLocal.prepareStatement(req2);
                    for (int i = 0; i < tIdRequete.length; i++) {
                        //,,
                        statementLocal.setString(1, tIdRequete[i]);
                        statementLocal.setString(2, Utilitaire.formatterDaty(tDaty[i]));
                        statementLocal.setString(3, tHeure[i]);
                        statementLocal.setString(4, connectionRow.getId());
                        statementLocal.executeUpdate();

                    }
                }
            }
            //LogSynchro.println("nivoka *********************** envoyeRequeteGroupe");

        } catch (Exception e) {
            if (connectionLocal != null) {
                connectionLocal.rollback();
            }
            e.printStackTrace();
            LogSynchro.println("INFO[" + connectionRow.getNom() + "]: " + e.getMessage());
            //throw e;
        } finally {
            
        }
    }

}
