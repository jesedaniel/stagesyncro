/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package periodictask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.util.Properties;
import java.util.Timer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import static periodictask.SynchroBase.connect;
import periodictask.ui.Monitor;
import periodictask.utils.CustomTime;
import periodictask.utils.Theme;

/**
 *  
 * @author Andy RANDRIANIRINA
 */
public class Synchronisation {
    public static boolean WITH_MONITOR              = false;
    public static boolean EXPORT_IMPORT             = false;
    public static final String PATH_TEMP            =System.getProperty("java.io.tmpdir")+"/synchrologging.log";
    public static final String PATH_STATIC          =Loader.load().getProperty("pathstatic");
    //public static final String PATH_CONFIG        ="D:\\Projects\\New-APJ\\New-APJ\\New-APJ-war\\src\\java\\periodictask\\config.properties";
    public static final String PATH_CONFIG          =""+Synchronisation.PATH_STATIC+"periodictask/config.properties"+"";
    //public static final String PATH_BUILDFILE     ="\"D:\\Projects\\New-APJ\\New-APJ\\New-APJ-war\\src\\java\\periodictask\\build.xml\"";
    public static final String PATH_BUILDFILE       ="\""+Synchronisation.PATH_STATIC+"periodictask/build.xml"+"\"";
    //public static final String PATH_RUNFILE       ="\"D:\\Projects\\New-APJ\\New-APJ\\New-APJ-war\\src\\java\\periodictask\\sync.bat\"";
    public static final String PATH_RUNFILE         ="\""+Synchronisation.PATH_STATIC+"periodictask/sync.bat"+"\"";
    public static final String WINDOWTITLE          ="85ceb46695acb5dfcc7a806904a25365";        //md5 de synchronisation, tokony soloina dynamique fa ny olana ilay any anaty .bat misoratra statique
    public static boolean IS_RUNNING=false;	
    public static ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    public static Runtime R = Runtime.getRuntime();
    
    private Process p;
    public static Monitor monitor;
    
    public static Synchronisation current_synchro=new Synchronisation();
    
    public static String CurrentDate() {
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        String currentTime = sdf.format(dt);
        return currentTime;
    }

    public static void main(String[] args) {
        Theme.refactorTheme(Theme.WINDOWS);
        Properties prop = new Properties();
        InputStream input = null;
        Timer timer = new Timer();
        try {
            prop.load(classLoader.getResourceAsStream("config.properties"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        String ipSource			=prop.getProperty("ipSource");
        String serverPort               =prop.getProperty("serverPort");
        String serverType               =prop.getProperty("serverType");
        String usernameSource           =prop.getProperty("usernameSource");
        String pwdSource		=prop.getProperty("pwdSource");                
        SynchroBase.ipsource 	=ipSource+":"+serverPort+":"+serverType;
        SynchroBase.username 	=usernameSource;
        SynchroBase.password 	=pwdSource;
        if(EXPORT_IMPORT){
            SynchroBaseExport.instance = new SynchroBaseExport();
            
            Connection  connection = null;                      //Already auto-commit false
               
            ArrayList<NamedServerConnection> namedServers = null;        //Already auto-commit false
            ArrayList<Server_Broadcast> servers = null;
            try{
                LogSynchro.println("INFO: Début de l'exportation");
                connection      = SynchroBase.GetConn();             

                //Alaina ny liste serveur cible
                servers         = SynchroBase.getServers(connection);
                LogSynchro.println("INFO: "+servers.size()+" Serveurs identifiées");  
                if(Synchronisation.WITH_MONITOR){
                    monitor = new Monitor();
                    Monitor.getInstance().setServers(servers);
                    Monitor.getInstance().refresh();
                    monitor.setVisible(true);
                }
                //Manokatra connexion par serveur            
                namedServers  = connect(servers);
                LogSynchro.println("INFO: Connection established ["+servers.size()+"] / ["+namedServers.size()+"]");
                SynchroBaseExport.instance.initExport(connection, namedServers);
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
        else{
            try {
                    //--------test time
                    String test_time = prop.getProperty("refreshtime");
                    CustomTime ct = CustomTime.format(test_time);
                    long millis = ct.toMillisecond();
                    System.out.println(millis);
                    //-----------------

                    SynchroBase s = new SynchroBase();
                    SynchroBase.setTime(millis);
                    timer.schedule(s, 0, millis);
                    if(WITH_MONITOR){
                        java.awt.EventQueue.invokeLater(new Runnable() {
                            public void run() {
                                try {
                                    monitor = new Monitor();
                                    monitor.setVisible(true);
                                } catch (Exception ex) {
                                    Logger.getLogger(Synchronisation.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        });
                    }
            } catch (Exception ex) {
                            timer.cancel();
                            ex.printStackTrace();	
                    } finally {
                    if (input != null) {
                            try {
                                    input.close();
                            } catch (IOException e) {
                                    timer.cancel();
                                    e.printStackTrace();
                            }
                    }
            }
        }
    }
    
    private void executeCommand(String command,boolean isRun) throws Exception{
        try { 
                System.out.println("Execution Command CMD");
                //Stop
                //this.stop();
                //creation du processus
                command = "cmd /c start "+command;
                if(isRun)
                    command = runFilePath();
                this.p = R.exec(command,null,null);
                
                InputStream in = p.getInputStream(); 
           
                //on récupère le flux de sortie du programme 

                StringBuilder build = new StringBuilder(); 
                char c = (char) in.read(); 

                while (c != (char) -1) { 
                    build.append(c); 
                    c = (char) in.read(); 
                }

                String response = build.toString(); 

                //on l'affiche 
                System.out.println(response);
                //this.p = Runtime.getRuntime().exec("cmd /c exit"); 
        } 
        catch (Exception e) { 
            throw e;
        } 
    }
    
    private static String getBuildRequest(){
        return target("");
    }
    
    private static String getRunRequest(){
        return target("run");
    }
    
    private static String target(String target){       
        return "ant -buildfile "+buildfilePath()+" "+target;
    }
    
    private static String buildfilePath(){
        return Synchronisation.PATH_BUILDFILE;   //Soloina dynamique
    }
    
    private static String runFilePath(){
        return Synchronisation.PATH_RUNFILE; //Tokony soloina dynamique
    }
    
    public void build()throws Exception{
        System.out.println("BUILD");
        executeCommand(getBuildRequest(),false);
    }
    
    public void build(String serverAddr,String serverPort,String serverType,String usernameSource,String pwdSource,String refreshtimesecond,String driver)throws Exception{
        saveParamChanges(serverAddr, serverPort, serverType, usernameSource, pwdSource, Integer.valueOf(refreshtimesecond), driver);
        build();
    }
    
    public void run()throws Exception{
        System.out.println("RUN");
        executeCommand(getRunRequest(),true);
    }
    
    public void stop()throws Exception{
        System.out.println("STOP");
        //executeCommand("");
        System.out.println("this.p = " + this.p);
        if(this.p!=null){
            String stopSynchro ="Task kill /F /T /FI \"WINDOWTITLE eq "+Synchronisation.WINDOWTITLE+"*\"";
            String stopSelect ="Task kill /F /T /FI \"WINDOWTITLE eq S\u00e9lection "+Synchronisation.WINDOWTITLE+"*\"";
            System.out.println("Stopping process");
            this.p.destroy();
            //this.p = this.p.destroyForcibly();
            System.out.println("stopSynchro = " + stopSynchro);
            System.out.println("stopSelect = " + stopSelect);
            this.p = Runtime.getRuntime().exec(stopSynchro);
            this.p = Runtime.getRuntime().exec(stopSelect);
            //this.p = Runtime.getRuntime().exec("cmd /c exit");
            System.out.println("Process stopped");
        }
            
    }
    
    public void clear()throws Exception{
        String file = Synchronisation.PATH_TEMP;
        try{
            File fileObject = new File(file);
            fileObject.delete();
        }     
        catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void saveParamChanges(String serverAddr, String serverPort, String serverType, String usernameSource, String pwdSource,int refreshtimesecond,String driver) {
        try {
            String ipSource = serverAddr+":"+serverPort+":"+serverType;
            Properties props = new Properties();
            ipSource = ipSource.replace("\\", "");
            
            props.setProperty("refreshtimesecond",  ""+refreshtimesecond);
            props.setProperty("ipSource",           ""+serverAddr);
            props.setProperty("serverPort",         ""+serverPort);
            props.setProperty("serverType",         ""+serverType);
            props.setProperty("usernameSource",     ""+usernameSource);
            props.setProperty("pwdSource",          ""+pwdSource);
            props.setProperty("driver",             ""+driver);
            
            File f = new File(Synchronisation.PATH_CONFIG.replaceAll("%20", " "));
            OutputStream out = new FileOutputStream(f);
            props.store(out, "Derniere modification: "+new Date());
            out.close();
        }
        catch (Exception e ){
            e.printStackTrace();
        }
    }
                        
    public static Properties load_default() {
        try {
            File f = new File(Synchronisation.PATH_CONFIG.replaceAll("%20", " "));
            FileInputStream fileInput = new FileInputStream(f);
            Properties properties = new Properties();
            properties.load(fileInput);
            fileInput.close();
            return properties;
        } catch (IOException ex) {
            Logger.getLogger(Loader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

