/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package periodictask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import periodictask.ui.Monitor;
import periodictask.utils.Constant;
import periodictask.utils.Utilitaire;

/**
 *
 * @author EBI
 */
public class SynchroBaseExport {
    
    public static SynchroBaseExport instance = null;
    
    public static String pathDirectory = "";
    public static String fileName = "";
    public static String fileImportPath = "";
    private FileOutputStream out = null;
    
    private String[] requests = null;
    
    private Connection connection = null; 
    private ArrayList<NamedServerConnection> connectionRows = null;
    
    public void initExport(Connection connection, ArrayList<NamedServerConnection> connectionRows){
        this.connection = connection;
        this.connectionRows = connectionRows;
		try {
			this.processExport();
		}
		catch (Exception ex) {
			ex.printStackTrace();	
        }
    }
    
    public void initImport() throws IOException, GeneralSecurityException{
        File inputFile = new File(fileImportPath);
        byte[] data = Files.readAllBytes(inputFile.toPath());
        FileInputStream fis = new FileInputStream(inputFile);
        fis.read(data, 0, data.length);
        fis.close();
        String decrypt = Utilitaire.decrypt(data);
        requests = decrypt.split(Constant.DELIMITER);
        Monitor.getInstance().setLabelSyncDataText(requests.length+"");
    }
    
    public void processExport() throws Exception{
        File file = new File(Monitor.getInstance().getFullPath());
        out = new FileOutputStream(file);
        ArrayList<HistoriqueSync> requests=null;
        //Pour chaque serveur cible
        if(connectionRows!=null && !connectionRows.isEmpty()){
            for(NamedServerConnection connectionRow:connectionRows){
                LogSynchro.println("INFO["+connectionRow.getNom()+"]: step one -- avant getHistoriqueSync");
                requests=SynchroBase.getHistoriqueSync(connectionRow);
                if(requests!=null && !requests.isEmpty()){
                    LogSynchro.println("INFO["+connectionRow.getNom()+"]: X: "+requests.size()+" requetes a éxporter");
                    SynchroBase.exportToFile(out, connectionRow, requests);
                }
                else{
                    LogSynchro.println("INFO["+connectionRow.getNom()+"]: Aucune requete a éxporter pour ce serveur.");
                }
            }
        }
        else{
            LogSynchro.println("WARNING: Aucune connexion etablie! Veuillez verifier la liste des serveurs");
        }
        LogSynchro.println("INFO: Ending step 1");
        out.flush();
        out.close();
    }
    
    public void processImport() throws Exception{
        
        HistoriqueSync hist = new HistoriqueSync();
        for(String r : requests){
            LogSynchro.println("IMPORTATION: " + Utilitaire.refactorNumbers(r.replace("\"", "'").replace(";", " ")) + "\n");
            if(SynchroBase.execute(connection, r)){
                LogSynchro.println("INFO: OK");
            }//*/
        }
    }
}
