/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package periodictask;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import periodictask.utilitaire.UtilDB;
import periodictask.utilitaire.Utilitaire;

/**
 *
 * @author pro
 */
public class RequeteAenvoyerGroupe extends RequeteAEnvoyer {

    private int etat;
    private String datyg;
    private String idrequeteAenvoyer;
    private ArrayList<RequeteAEnvoyer> tRequeteAEnvoyer;

    public RequeteAenvoyerGroupe(ArrayList<RequeteAEnvoyer> tRequeteAEnvoyer) {
        this.settRequeteAEnvoyer(tRequeteAEnvoyer);
        String idrequeteAenvoyere = "", actione = "", tableconcernee = "", requetee = "", datye = "", heuree = "", serveure = "";
        for (RequeteAEnvoyer requests : this.gettRequeteAEnvoyer()) {
            idrequeteAenvoyere += requests.getId() + ";;";
            actione += requests.getAction() + ";;";
            tableconcernee += requests.getTableconcerne() + ";;";
            requetee += requests.getRequete() + ";;";
            datye += requests.getDaty() + ";;";
            heuree += requests.getHeure() + ";;";
            serveure += requests.getServeur() + ";;";
        }
        this.setAction(actione);
        this.setRequete(requetee);
        this.setDatyg(datye);
        this.setTableconcerne(tableconcernee);
        this.setHeure(heuree);
        this.setServeur(serveure);
        this.setEtat(1);
        this.setIdrequeteAenvoyer(idrequeteAenvoyere);        
    }

    public RequeteAenvoyerGroupe(String idrequeteAenvoyer, String action, String tableconcerne, String requete, String datyg, String heure, String serveur) {
        this.setIdrequeteAenvoyer(idrequeteAenvoyer);
        this.setAction(action);
        this.setHeure(heure);
        this.setDatyg(datyg);
        this.setTableconcerne(tableconcerne);
        this.setRequete(requete);
        this.setServeur(serveur);
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getDatyg() {
        return datyg;
    }

    public void setDatyg(String datyg) {
        this.datyg = datyg;
    }

    public ArrayList<RequeteAEnvoyer> gettRequeteAEnvoyer() {
        return tRequeteAEnvoyer;
    }

    public void settRequeteAEnvoyer(ArrayList<RequeteAEnvoyer> tRequeteAEnvoyer) {
        this.tRequeteAEnvoyer = tRequeteAEnvoyer;
    }

    public String getIdrequeteAenvoyer() {
        return idrequeteAenvoyer;
    }

    public void setIdrequeteAenvoyer(String idrequeteAenvoyer) {
        this.idrequeteAenvoyer = idrequeteAenvoyer;
    }
    public static PreparedStatement getPrepared(Connection con) throws Exception
    {
        String query = "INSERT INTO requete_a_envoyer_groupe (ID,ACTION,TABLECONCERNE,ETAT,DATYG,HEURE,SERVEUR,IDREQUETEAENVOYER,REQUETE) VALUES ('RAEG00'||getseqrequete_a_envoyer_groupe,?,?,?,?,?,?,?,?)";
        return con.prepareCall(query);
    }
    public RequeteAenvoyerGroupe inserer(PreparedStatement insertHistoSync) throws Exception {
        LogSynchro.println("=========================grouperRequeteAenvoyer Start tttt");
        
        try {
            //RequeteAenvoyerGroupe requeteGroupe = null;
            /*if (this.gettRequeteAEnvoyer().size() > 0) {
                String idrequeteAenvoyer = "", action = "", tableconcerne = "", requete = "", daty = "", heure = "", serveur = "";
                for (RequeteAEnvoyer requests : this.gettRequeteAEnvoyer()) {
                    idrequeteAenvoyer += requests.getId() + ";;";
                    action += requests.getAction() + ";;";
                    tableconcerne += requests.getTableconcerne() + ";;";
                    requete += requests.getRequete() + ";;";
                    daty += requests.getDaty() + ";;";
                    heure += requests.getHeure() + ";;";
                    serveur += requests.getServeur() + ";;";
                    System.out.println("requests = " + requests.getRequete());
                }*/

                //requeteGroupe = new RequeteAenvoyerGroupe(idrequeteAenvoyer, action, tableconcerne, requete, daty, heure, serveur);
                //insertHistoSync.setString(1, "");
                
                insertHistoSync.setString(1, getAction());
                insertHistoSync.setString(2, getTableconcerne());
                insertHistoSync.setInt(3, this.getEtat());
                insertHistoSync.setString(4, getDatyg());
                insertHistoSync.setString(5, getHeure());
                insertHistoSync.setString(6, getServeur());
                insertHistoSync.setString(7, getIdrequeteAenvoyer());
                insertHistoSync.setString(8, getRequete());
                insertHistoSync.executeUpdate();
            
            LogSynchro.println("=========================grouperRequeteAenvoyer End");
            return this;

        } catch (Exception e) {
            e.printStackTrace();
            insertHistoSync.getConnection().rollback();
            
        } 
        return this;
    }

    public RequeteAenvoyerGroupe grouperRequeteAenvoyer(Connection con) throws Exception {
        LogSynchro.println("=========================grouperRequeteAenvoyer Start");

        PreparedStatement insertHistoSync = null;
        try {
            RequeteAenvoyerGroupe requeteGroupe = null;

            if (this.gettRequeteAEnvoyer().size() > 0) {
                String idrequeteAenvoyer = "", action = "", tableconcerne = "", requete = "", daty = "", heure = "", serveur = "";
                for (RequeteAEnvoyer requests : this.gettRequeteAEnvoyer()) {
                    idrequeteAenvoyer += requests.getId() + ";;";
                    action += requests.getAction() + ";;";
                    tableconcerne += requests.getTableconcerne() + ";;";
                    requete += requests.getRequete() + ";;";
                    daty += requests.getDaty() + ";;";
                    heure += requests.getHeure() + ";;";
                    serveur += requests.getServeur() + ";;";
                }

                requeteGroupe = new RequeteAenvoyerGroupe(idrequeteAenvoyer, action, tableconcerne, requete, daty, heure, serveur);
                String query = "INSERT INTO requete_a_envoyer_groupe (ID,ACTION,TABLECONCERNE,REQUETE,DATYG,HEURE,SERVEUR,IDREQUETEAENVOYER,ETAT) VALUES ('RAEG00'||getseqrequete_a_envoyer_groupe,?,?,?,?,?,?,?,?)";
                insertHistoSync = con.prepareCall(query);
//            insertHistoSync.setString(1, "");
                insertHistoSync.setString(1, requeteGroupe.getAction());
                insertHistoSync.setString(2, requeteGroupe.getTableconcerne());
                insertHistoSync.setString(3, requeteGroupe.getRequete());
                insertHistoSync.setString(4, requeteGroupe.getDatyg());
                insertHistoSync.setString(5, requeteGroupe.getHeure());
                insertHistoSync.setString(6, requeteGroupe.getServeur());
                insertHistoSync.setString(7, requeteGroupe.getIdrequeteAenvoyer());
                insertHistoSync.setInt(8, 1);
                insertHistoSync.executeUpdate();
            }
            LogSynchro.println("=========================grouperRequeteAenvoyer End");
            return requeteGroupe;

        } catch (Exception e) {
            if (con != null) {
                con.rollback();
            }
        } finally {
            if (insertHistoSync != null) {
                insertHistoSync.close();
            }
        }
        return null;
    }

}
