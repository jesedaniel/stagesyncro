/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package periodictask;

import java.util.ArrayList;

/**
 *
 * @author pro
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String fichier = "texte.txt";
        ArrayList<String> f = Lecture.lireFichier(fichier);
        try{
            if(f.size() > 0)
            {
                if(f.get(0).compareToIgnoreCase("false") == 0)
                {
                    Lecture.ecrireFichier(fichier,"true");
                    SynchroBase.execute();
                    Lecture.ecrireFichier(fichier,"false");
                }
                else
                {
                    System.out.println("En cours.");
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
            Lecture.ecrireFichier(fichier,"false");
        }
        finally
        {
            Lecture.ecrireFichier(fichier,"false");
        }
    }
    
}
