/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package periodictask.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/**
 *
 * @author EBI
 */
public abstract class Utilitaire {
    
    private final static String password = "123456";
    private final static String salt = "pk19-EGS";
    private final static int iterationCount = 20;
    private final static String algorithm = "PBEWithMD5AndDES";
    
    public static byte[] encrypt(String property) throws GeneralSecurityException, UnsupportedEncodingException {
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(algorithm);
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(password.toCharArray()));
        Cipher pbeCipher = Cipher.getInstance(algorithm);
        pbeCipher.init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(salt.getBytes(), iterationCount));
        return pbeCipher.doFinal(property.getBytes("UTF-8"));
    }

    public static String decrypt(byte[] property) throws GeneralSecurityException, IOException {
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(algorithm);
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(password.toCharArray()));
        Cipher pbeCipher = Cipher.getInstance(algorithm);
        pbeCipher.init(Cipher.DECRYPT_MODE, key, new PBEParameterSpec(salt.getBytes(), iterationCount));
        return new String(pbeCipher.doFinal(property));
    }

    public static String refactorNumbers(String request) {
        String res = request;
        final Pattern pattern = Pattern.compile("\\'\\d+(\\.\\d+)?([eE]?[-+]?[0-9]+)?\\'", Pattern.DOTALL);
        final Matcher matcher = pattern.matcher(res);
        while(matcher.find()){
            String rep = matcher.group().replace("'", "");
            res = res.replace(matcher.group(), rep);
        }
        return res;
    }
}
