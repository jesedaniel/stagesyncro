/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parameters;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Andy
 */
public class Template {
    public static  String PROJECT_COLOR = "blue";
    public static final String PROJECT_BUTTON_COLOR_DEFAULT = "btn-info";
    public static final String PROJECT_TEXT_COLOR_DEFAULT = "text-info";
    public static final String PROJECT_PAGINATION_COLOR_DEFAULT = "pagination-info";
    
    public static final String CHEMIN_DIST = "/assets";
    public static final String CHEMIN_IMG = CHEMIN_DIST + "/img";
    public static final String CHEMIN_CSS = CHEMIN_DIST + "/css";
    public static final String CHEMIN_JS = CHEMIN_DIST + "/js";
    
    public final static List<String> getListCSS(){
        List<String> listCSS = new ArrayList<>();
        listCSS.add("bootstrap.min.css");
        listCSS.add("material-dashboard.css");
        listCSS.add("demo.css");
        listCSS.add("font-awesome.min.css");
        listCSS.add("font-google-analysis.css");
        listCSS.add("styleCustom.css");
        return listCSS;
    }
    
    public final static List<String> getListJS(){
        List<String> listJS = new ArrayList<>();
        //listJS.add("analytics.js");
        listJS.add("moment.min.js");
        listJS.add("jquery-3.1.1.min.js");
        listJS.add("bootstrap-datetimepicker.js");
        listJS.add("bootstrap-notify.js");
        listJS.add("bootstrap.min.js");
        listJS.add("chartist.min.js");
        listJS.add("demo.js");
        listJS.add("fullcalendar.min.js");
        listJS.add("jasny-bootstrap.min.js");
        listJS.add("jquery-jvectormap.js");
        listJS.add("jquery-ui.min.js");
        listJS.add("jquery.bootstrap-wizard.js");
        listJS.add("jquery.datatables.js");
        listJS.add("jquery.select-bootstrap.js");
        listJS.add("jquery.sharrre.js");
        listJS.add("jquery.tagsinput.js");
        listJS.add("jquery.validate.min.js");
        listJS.add("material-dashboard.js");
        listJS.add("material.min.js");
        listJS.add("nouislider.min.js");
        listJS.add("perfect-scrollbar.jquery.min.js");
        listJS.add("sweetalert2.js");
        return listJS;
    }
}
