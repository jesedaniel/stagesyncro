/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parameters;

/**
 *
 * @author Andy
 */
public class Projet {
    public static final String NAME_PROJECT = "Sync";
    public static final String INITIAL_NAME_PROJET = createInitialProject();
    
    public static final String MAIN_LAYOUT = "module.jsp";
    
    public static final String NAME_DEPLOYEMENT = "synchronisation";
    
    public static final String GET_LINK_SOURCE = NAME_DEPLOYEMENT+"/pages/"+MAIN_LAYOUT+"?but=";
            
    public static String createInitialProject(){
        String[] nameProjet = NAME_PROJECT.split(" ");
        String retour = "";
        for(String value : nameProjet){
            retour += String.valueOf(value.charAt(0));
        }
        return retour;
    }
}
