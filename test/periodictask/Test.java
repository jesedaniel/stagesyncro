/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package periodictask;

import java.sql.*;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import oracle.ucp.jdbc.PoolDataSource;
import periodictask.utils.CustomTime;

/**
 *
 * @author EBI
 */
public class Test {
    public static void main(String args[]) throws SQLException {
        test0();
    }
    
    static void test0(){
        CustomTime ct = new CustomTime(1,1,1,1);
        long millis = ct.toMillisecond();
        System.out.println(millis);
    }
    
    /*static void test1(){
        try
        {
            //Creating a pool-enabled data source
            PoolDataSource pds = PoolDataSourceFactory.getPoolDataSource();
            //Setting connection properties of the data source
            pds.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
            pds.setURL("jdbc:oracle:thin:@//26.16.158.217:1521/orcl");
            pds.setUser("vvligne");
            pds.setPassword("vedgtgt987");
            //Setting pool properties
            pds.setInitialPoolSize(5);
            pds.setMinPoolSize(5);
            pds.setMaxPoolSize(10);
            //Borrowing a connection from the pool
            Connection conn = pds.getConnection();
            System.out.println("\nConnection borrowed from the pool");
            //Checking the number of available and borrowed connections
            int avlConnCount = pds.getAvailableConnectionsCount();
            System.out.println("\nAvailable connections: " + avlConnCount);
            int brwConnCount = pds.getBorrowedConnectionsCount();
            System.out.println("\nBorrowed connections: " + brwConnCount);
            //Working with the connection
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM HISTORIQUE_SYNC WHERE ETAT <11 ORDER BY id asc");
            while(rs.next())
             System.out.println("\nConnected as: "+rs.getString(1));
            rs.close();
            //Returning the connection to the pool
            conn.close();
            conn=null;
            System.out.println("\nConnection returned to the pool");
            //Checking the number of available and borrowed connections again
            avlConnCount = pds.getAvailableConnectionsCount();
            System.out.println("\nAvailable connections: " + avlConnCount);
            brwConnCount = pds.getBorrowedConnectionsCount();
            System.out.println("\nBorrowed connections: " + brwConnCount);
        }
        catch(SQLException e)
        {
            System.out.println("\nAn SQL exception occurred : " + e.getMessage());
        }
    }//*/
}
