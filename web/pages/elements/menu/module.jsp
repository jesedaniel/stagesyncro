<%@page import="Parameters.Projet"%>
<div class="logo">
    <a href="#" class="simple-text">
        <%=Projet.NAME_PROJECT%>
    </a>
</div>
<div class="logo logo-mini">
    <a href="#" class="simple-text">
        <%=Projet.INITIAL_NAME_PROJET%>
        <%
            String lien = "module.jsp";
        %>
    </a>
</div>
<div class="sidebar-wrapper">
    <div class="user">
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                <b class="caret"></b>
            </a>
            <div class="collapse" id="collapseExample">
                <ul class="nav">
                    <li>
                        <a href="#">Paramètres</a>
                    </li>
                    <li>
                        <a href="<%=lien+"?but=asynctask/synchronisation.jsp"%>">Synchronisation</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
   
</div>