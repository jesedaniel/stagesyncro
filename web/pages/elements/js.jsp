<%@page import="Parameters.Projet"%>
<%@page import="Parameters.Template"%>
<%
    for (String js : Template.getListJS()) {
%>
<script src="<%=request.getContextPath() + Template.CHEMIN_JS + "/" + js%>" type="text/javascript"></script>
<%
    }
%>

<script type="text/javascript">
    $().ready(function () {
        demo.checkFullPageBackgroundImage();

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>

<script>
    function verifEditerTef(et, name) {
        if (et < 11) {
            alert('Impossible d\'editer Tef. ' + name + ' non vis� ');
        } else {
            document.tef.submit();

        }
    }
    function verifLivraisonBC(et) {
        if (et < 11) {
            alert('Impossible d effectuer la livraison du bon de commande');
        } else {
            document.tef.submit();

        }
    }
    function CocherToutCheckBox(ref, name) {
        var form = ref;

        while (form.parentNode && form.nodeName.toLowerCase() != 'form') {
            form = form.parentNode;
        }

        var elements = form.getElementsByTagName('input');

        for (var i = 0; i < elements.length; i++) {
            if (elements[i].type == 'checkbox' && elements[i].name == name) {
                elements[i].checked = ref.checked;
            }
        }
    }
    function pagePopUp(page, width, height) {
        w = 750;
        h = 600;
        t = "D&eacute;tails";

        if (width != null || width == "")
        {
            w = width;
        }
        if (height != null || height == "") {
            h = height;
        }
        window.open(page, t, "titulaireresizable=no,scrollbars=yes,location=no,width=" + w + ",height=" + h + ",top=0,left=0");
    }
    function searchKeyPress(e)
    {
        // look for window.event in case event isn't passed in
        e = e || window.event;
        if (e.keyCode == 13)
        {
            document.getElementById('btnListe').click();
            return false;
        }
        return true;
    }
    function getChoix() {
        setTimeout("document.frmchx.submit()", 800);
    }
</script>
<script src="${pageContext.request.contextPath}/apjplugins/champcalcul.js" ></script>      
<script src="${pageContext.request.contextPath}/apjplugins/champdate.js" ></script>      
<script src="${pageContext.request.contextPath}/apjplugins/champautocomplete.js" ></script>  
<script language="javascript">
    (function ($) {
        var title = ($('h1:first').text());
        if (title === '' || title == null)
            title =  ($('h2:first').text());
        if (title === '' || title == null)
            title = '<%=Projet.NAME_PROJECT%>'; 
        document.title = title;
    }(jQuery));
</script>

<script type="text/javascript">
    $(document).ready(function() {
        demo.initFormExtendedDatetimepickers();
    });
</script>