<%@page import="java.util.Properties"%>
<%@page import="periodictask.Synchronisation"%>
<%@page import="periodictask.Loader"%>
<%@page import="Parameters.Template"%>
<%@page import="Parameters.Projet"%>
   
<%
    try{
    Properties prop = Synchronisation.load_default();
    String url = request.getContextPath()+"/pages/log.jsp";
    String sync = request.getContextPath()+"/pages/sync.jsp";
    
    String value_host       =prop.getProperty("ipSource");;
    String value_port       =prop.getProperty("serverPort");
    String value_login      =prop.getProperty("usernameSource");
    String value_password   =prop.getProperty("pwdSource");
    String value_sid        =prop.getProperty("serverType");
    String value_driver     =prop.getProperty("driver");
    
    int value_refresh       =(Integer.valueOf(prop.getProperty("refreshtimesecond")));
%>
<script src="<%=request.getContextPath() + Template.CHEMIN_JS%>/jquery-3.1.1.min.js" type="text/javascript"></script>
<div class="row" style="margin-top: 70px;">
    <div class="col-md-12">
        <div class="card">
            <div class='card-header collapsed' data-background-color="<%=Template.PROJECT_COLOR%>">
                <h4>Module de Synchronisation</h4>
            </div>
                
            <div class="card-content">
                <div class="row">
                    
                    <div class="col-sm-3 pull-right"> 
                        <a class="btn btn-white"   href="#" onclick="clear_console();"><i class="material-icons">memory</i> Effacer Console</a>
                    </div>
                    <div class="col-sm-3 pull-right"> 
                        <a class="btn btn-danger"   href="#" onclick="stop();"><i class="material-icons">stop</i> Arr�ter Synchronisation</a>
                    </div>
                    <div class="col-sm-3 pull-right">
                        <a class="btn btn-success"  href="#" onclick="run();"><i class="material-icons">code</i> Commencer Synchronisation</a>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header" data-background-color="<%=Template.PROJECT_COLOR%>">
                                <h4>Configuration</h4>
                            </div>
                            <div class="card-content" style="height:429px;padding: 10px;margin:10px;">                                                             
                               <div class="row">
                                   <div class="col-md-8">
                                        <label for="servAddr" class="control-label">Host:</label>
                                        <input name="servAddr" type="textbox" class="form-control" onkeydown='return searchKeyPress(event)' placeholder="127.0.0.1" id="servAddr" value="<%=value_host%>">
                                   </div>
                                   <div class="col-md-4">
                                       <label for="servAddr" class="control-label">Port:</label>
                                       <input name="serverPort" type="textbox" class="form-control"  onkeydown='return searchKeyPress(event)' placeholder="1521" id="serverPort" value="<%=value_port%>" >
                                   </div>
                                </div>                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="usernameSource" class="control-label">Identifiant:</label>
                                        <input name="usernameSource" type="textbox" class="form-control" onkeydown='return searchKeyPress(event)' id="usernameSource" value="<%=value_login%>">
                                    </div>   
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pwdSource" class="control-label">Mot de passe:</label>
                                        <input type="password" name="pwdSource" class="form-control" id="pwdSource" id="pwdSource" value="<%=value_password%>"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="serverType" class="control-label">Service SID:</label>
                                        <!--<select name="serverType" class="form-control" id="serverType">
                                            <option value="orcl">ORCL</option>
                                            <option value="xe">XE</option>
                                        </select>-->
                                        <input name="serverType" type="textbox" class="form-control" value="<%=value_sid%>" onkeydown='return searchKeyPress(event)' id="serverType" >
                                    </div>
                                    <div class="col-md-4">
                                        <label for="refreshtimesecond" class="control-label">Refresh (Sec): </label>
                                        <input type="number" name="refreshtimesecond" class="form-control col-md-1" id="refreshtimesecond" min="10" max="5000" value="<%=value_refresh%>"/>
                                    </div>
                                </div>
                                <input type="hidden" value="<%=value_driver%>" placeholder="oracle.jdbc.driver.OracleDriver" name="driver" id="driver"/>                                 
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <a class="btn"  href="#" onclick="build();"><i class="material-icons">save</i> Enregistrer Configuration</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <code class="col-md-8" style="height:600px;padding: 10px;margin:10px;overflow-y:scroll;background: #080808;color:#00acc1;" id="code">
                                            
                    </code>
                </div>
            </div>
            </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {        
        setInterval(function() {
            getData();
      }, 1000);
    });
    
    function getData(){
        $.ajax({
            type: 'GET',
            dataType : 'html',
            url: '<%=url%>',
            success: function(response){
                //console.log('Success');
                //console.log(response);
                $('#code').html(response);
                
            },
            error:function (jqXHR,textStatus,errorThrown ){
                console.log('Error');
                console.log(errorThrown);
            }
        });
    }
    
    function build(){
        console.log("Manao build");
        $.ajax({
            type: 'POST',
            url: '<%=sync%>',
            data:{
                servAddr:$('#servAddr').val(),
                serverPort:$('#serverPort').val(),
                serverType:$('#serverType').val(),
                usernameSource:$('#usernameSource').val(),
                pwdSource:$('#pwdSource').val(),
                refreshtimesecond:$('#refreshtimesecond').val(),
                driver:$('#driver').val(),
                sync:'2'
            },
            success: function(response){
                console.log('Build');
            },
            error:function (jqXHR,textStatus,errorThrown ){
                console.log('Erreur build');
            }
        });
    }
    
    function run(){
        $.ajax({
            type: 'POST',
            url: '<%=sync%>',
            data:{
                sync:'1'
            },
            success: function(response){
                console.log('run');
            },
            error:function (jqXHR,textStatus,errorThrown ){
                console.log('Erreur run');
            }
        });
    }
    
    function stop(){
        $.ajax({
            type: 'POST',
            url: '<%=sync%>',
            data:{
                sync:'0'
            },
            success: function(response){
                console.log('stop');
            },
            error:function (jqXHR,textStatus,errorThrown ){
                console.log('Erreur stop');
            }
        });
    }
    
    function clear_console(){
        $.ajax({
            type: 'POST',
            url: '<%=sync%>',
            data:{
                sync:'-1'
            },
            success: function(response){
                console.log('cleared');
                getData();
                //console.log('stop');
            },
            error:function (jqXHR,textStatus,errorThrown ){
                console.log('Erreur stop');
            }
        });
    }
</script>
<%
    }
    catch(Exception e){
        e.printStackTrace();
        out.println("<script language='JavaScript'> alert('"+e.getMessage()+"');</script>");
    }
%>