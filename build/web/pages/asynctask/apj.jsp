<%@page import="bean.CGenUtil"%>
<%@page import="bean.TypeObjet"%>
<%@page import="Parameters.Template"%>
<%@page import="Parameters.Projet"%>
<%
    String url      = request.getContextPath()+"/pages/generator.jsp";
    String url_map  = request.getContextPath()+"/pages/mappage.jsp";
    
    TypeObjet table_disponible = new TypeObjet();
    table_disponible.setNomTable("TABLE_DISPONIBLE");
    TypeObjet[] table_disponibles = (TypeObjet[])CGenUtil.rechercher(table_disponible, null, null, " and desce='TABLE' ");
    
    String table_source = "[";  String view_source = "[";
    
    for(TypeObjet row:table_disponibles){
        table_source+="'"+row.getVal()+"',";
    }
   
    if(table_source.length()>0)   
        table_source=table_source.substring(0,table_source.length()-1);
    
    table_source+="]";
    
    
    
    TypeObjet[] view_disponibles = (TypeObjet[])CGenUtil.rechercher(table_disponible, null, null, " and desce='VIEW' ");
    
    for(TypeObjet row:view_disponibles){
        view_source+="'"+row.getVal()+"',";
    }
   
    if(view_source.length()>0)   
        view_source=view_source.substring(0,view_source.length()-1);    
    view_source+="]";
    
    //System.out.println("###"+view_source);
   
%>
<div class="row" style="margin-top: 70px;">
    <div class="col-md-12">
        <div class="card">
            <div class="card-content">
                <div class="wizard-container">
                    <div class="card wizard-card" data-color="rose" id="wizardProfile">
                        <form id="apjform" action="<%=url%>">
                                <div class="wizard-header">
                                    <h3 class="wizard-title">
                                        G�n�rateur APJ
                                    </h3>
                                    <h5>Veuillez suivre l'assistant suivant pour cr�er les fichiers *.jsp</h5>
                                </div>

                                <div class="wizard-navigation">
                                    <ul>
                                        <li>
                                            <a href="#step1" data-toggle="tab">Sources</a>
                                        </li>
                                        <li>
                                            <a href="#step2" data-toggle="tab">Pages</a>
                                        </li>
                                        <li>
                                            <a href="#step3" data-toggle="tab">Champs</a>
                                        </li>
                                        <li>
                                            <a href="#step4" data-toggle="tab">Autres</a>
                                        </li>
                                        <!--<li>
                                            <a href="#step5" data-toggle="tab">Etape 5</a>
                                        </li>-->
                                    </ul>
                                </div>
                                <div class="tab-content" style="height: 650px;">
                                    <div class="tab-pane" id="step1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-5" id="soure_table_radio_parent">                                                    
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="source_table_radio" checked="true" value="0"> Table
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="source_table_radio" value="1"> Vue (Saisie et Modif non disponible)
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="source_table_radio" value="2"> Nouvelle table
                                                            </label>
                                                        </div>
                                                        <input type="hidden" id="source_table_radio" value="0"/>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Table ou Vue source
                                                                <small></small>
                                                            </label>
                                                            <input name="source_table_text" id="source_table_text" type="text" value=" " class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-3 col-md-offset-2">
                                                        <a href="#" class="btn btn-sm" id="source_classe_import">        
                                                            Importer package
                                                        </a>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Classe de base
                                                                <small></small>
                                                            </label>
                                                            <input name="source_classe"  id="source_classe" type="text" class="form-control" value=" ">
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    
                                                    <div class="col-md-5">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="newmap_database" value="0" checked> Par d�faut
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="newmap_database" value="0"> Oracle
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="newmap_database" value="1"> PostgreSQL
                                                            </label>
                                                        </div> 
                                                    </div>
                                                    
                                                    <div class="col-md-5">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label" for="newmap_mapping">Package nouvelle classe
                                                                <small></small>
                                                            </label>
                                                            <input name="newmap_mapping" id="newmap_mapping" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                                                                       
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <label class="control-label" for="newmap_script">Script CREATE table � mapper
                                                            <small></small>
                                                        </label>
                                                        <textarea name="newmap_script" id="newmap_script" cols="40" rows="5" style=" 
                                                                display: block;
                                                                width: 100%;
                                                                height:200px;
                                                                resize: none;
                                                                border: 0;
                                                                overflow-y: scroll;
                                                                padding: 10px 5px;
                                                                margin-bottom:40px;
                                                                
                                                                background: white no-repeat;                                                        
                                                                background-image: linear-gradient(to bottom, #1abc9c, #1abc9c), linear-gradient(to bottom, silver, silver);
                                                                background-size: 0 2px, 100% 1px;     
                                                                transition: background-size 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);"></textarea>
                                                        <!--<input name="newmap_script" id="newmap_script" type="textarea" class="form-control" style="
                                                            display: block;
                                                            height: 100px;                                                                                                               
                                                            " rows="5"cols="40" >-->
                                                    </div>
                                                </div>
                                                <div class="row">                                           
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="col-md-12">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" id="newmap_function" name="newmap_function">Script Fonction s�quence
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" id="newmap_sequence" name="newmap_sequence">Script S�quence
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">                                                               
                                                            <select name="newmap_parent" id="newmap_parent" class="form-control" id="serverType">
                                                                <option value="0">ClassMAPTable</option>
                                                                <option value="1">ClassEtat</option>
                                                                <option value="2">ClassUser</option>
                                                            </select>                                                        
                                                        </div>
                                                    </div>
                                                </div>       
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Crit�res (Utiliser ',' comme s�parateur)
                                                            <small></small>
                                                    </label>
                                                    <input name="source_criteria" id="source_criteria" value=" " type="text" class="form-control">
                                                </div>
                                            </div>  
                                            <div class="col-md-5 col-md-offset-1">                                                  
                                                
                                                <div class="form-group label-floating">
                                                    <label for="newmap_destination" class="control-label">Chemin Destination des fichiers
                                                            <small></small>
                                                    </label>
                                                    <input name="newmap_destination" id="newmap_destination" type="text" class="form-control">
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-1">
                                                <a href="#" id="newmap_submit" class="btn btn-sm">        
                                                    <i class="fa fa-plus"></i> Mapper
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="step2">
                                            <h5>Choisir le ou les types de fichiers � cr�er</h5>
                                                <div class="col-lg-10 col-lg-offset-2">
                                                  <div class="col-sm-2">
                                                      <div class="choice" data-toggle="wizard-checkbox">
                                                          <input type="checkbox" name="apj_file_type_liste" id="apj_file_type_liste" value="Liste">
                                                          <div class="icon">
                                                              <i class="fa fa-th-list"></i>
                                                          </div>
                                                          <h6>Liste</h6>
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-2">
                                                      <div class="choice" data-toggle="wizard-checkbox">
                                                          <input type="checkbox" name="apj_file_type_analyse" id="apj_file_type_analyse" value="Analyse">
                                                          <div class="icon">
                                                              <i class="fa fa-search"></i>
                                                          </div>
                                                          <h6>Analyse</h6>
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-2">
                                                      <div class="choice" data-toggle="wizard-checkbox">
                                                          <input type="checkbox" name="apj_file_type_fiche" id="apj_file_type_fiche" value="Fiche">
                                                          <div class="icon">
                                                              <i class="fa fa-file"></i>
                                                          </div>
                                                          <h6>Fiche</h6>
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-2">
                                                      <div class="choice" data-toggle="wizard-checkbox">
                                                          <input type="checkbox" name="apj_file_type_saisie" id="apj_file_type_saisie" class="anti_view" value="Saisie">
                                                          <div class="icon">
                                                              <i class="fa fa-pencil-square-o"></i>
                                                          </div>
                                                          <h6>Saisie</h6>
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-2">
                                                      <div class="choice" data-toggle="wizard-checkbox">
                                                          <input type="checkbox" name="apj_file_type_modif" id="apj_file_type_modif" class="anti_view" value="Modification">
                                                          <div class="icon">
                                                              <i class="fa fa-refresh"></i>
                                                          </div>
                                                          <h6>Modification</h6>
                                                      </div>
                                                  </div>
                                              </div>  

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <h5>Fichier</h5>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <small>
                                                                <div class="togglebutton">
                                                                    <label>
                                                                        Simple&nbsp;&nbsp;<input type="checkbox" id="simple_advanced" style="background-color: #e91e63!important;"> Avanc�e
                                                                    </label>
                                                                </div>
                                                            </small>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="row file_prefixe_simple">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label">Pr�fixe du fichier
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_prefixe" id="file_prefixe" type="text"  value=" "  class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row file_prefixe_advanced simple_advanced" style="display:none;">
                                                            <div class="form-group label-floating l_base">
                                                                <label class="control-label">Nom du fichier liste
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_prefixe_liste" id="file_prefixe_liste" type="text" value=" " class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating a_base">
                                                                <label class="control-label">Nom du fichier analyse
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_prefixe_analyse" id="file_prefixe_analyse" type="text" value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating f_base">
                                                                <label class="control-label">Nom du fichier fiche
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_prefixe_fiche" id="file_prefixe_fiche" type="text"  value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating s_base">
                                                                <label class="control-label">Nom du fichier saisie
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_prefixe_saisie" id="file_prefixe_saisie" type="text"  value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating m_base">
                                                                <label class="control-label">Nom du fichier modification
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_prefixe_modif" id="file_prefixe_modif" type="text" value=" "  class="form-control ">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-md-offset-1">
                                                        <div class="row file_title_simple">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label">Titre
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_title" id="file_title" value=" "  type="text" class="form-control">
                                                            </div>
                                                        </div>

                                                        <div class="row file_title_advanced simple_advanced" style="display:none;">
                                                            <div class="form-group label-floating l_base">
                                                                <label class="control-label">Liste titre
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_title_liste" id="file_title_liste" type="text" value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating a_base">
                                                                <label class="control-label">Analyse titre
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_title_analyse" id="file_title_analyse" type="text"  value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating f_base">
                                                                <label class="control-label">Fiche titre
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_title_fiche" id="file_title_fiche" type="text" value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating s_base">
                                                                <label class="control-label">Saisie titre
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_title_saisie" id="file_title_saisie" type="text" value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating m_base">
                                                                <label class="control-label">Modification titre
                                                                    <small></small>
                                                                </label>
                                                                <input name="file_title_modif" id="file_title_modif" type="text" value=" "  class="form-control ">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-3 col-md-offset-1">
                                                        <div class="row table_name_simple">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label">Table
                                                                    <small></small>
                                                                </label>
                                                                <input name="table_name" id="table_name" value=" "  type="text" class="form-control">
                                                            </div>
                                                        </div>

                                                        <div class="row table_name_advanced simple_advanced" style="display:none;">
                                                            <div class="form-group label-floating l_base">
                                                                <label class="control-label">Liste Table
                                                                    <small></small>
                                                                </label>
                                                                <input name="table_name_liste" id="table_name_liste" type="text" value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating a_base">
                                                                <label class="control-label">Analyse Table
                                                                    <small></small>
                                                                </label>
                                                                <input name="table_name_analyse" id="table_name_analyse" type="text"  value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating f_base">
                                                                <label class="control-label">Fiche Table
                                                                    <small></small>
                                                                </label>
                                                                <input name="table_name_fiche" id="table_name_fiche" type="text" value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating s_base">
                                                                <label class="control-label">Saisie Table
                                                                    <small></small>
                                                                </label>
                                                                <input name="table_name_saisie" id="table_name_saisie" type="text" value=" "  class="form-control ">
                                                            </div>
                                                            <div class="form-group label-floating m_base">
                                                                <label class="control-label">Modification Table
                                                                    <small></small>
                                                                </label>
                                                                <input name="table_name_modif" id="table_name_modif" type="text" value=" "  class="form-control ">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        
                                    </div>
                                    <div class="tab-pane" id="step3">                                        
                                        
                                        <div class="col-lg-12">
                                                <h5>Simple</h5>
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Champ</th>                                                               
                                                                <th>Afficher</th>                           
                                                                <th>Lien</th>                                       
                                                                <th>Somme</th>
                                                                <th>Filtrer</th>
                                                                <th>Tableau</th>
                                                                <th>Intervalle</th>
                                                                <th>Type Objet</th>
                                                                <th>Pop-up</th>                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody id="champ_body"></tbody>
                                                    </table>
                                                </div>
                                        </div>
                                       
                                    </div>
                                    
                                    <div class="tab-pane" id="step4">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h5>Autres options</h5>
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Champ</th>
                                                                <th>Autre Liste</th>
                                                                <th>Autre Analyse</th>
                                                                <th>Autre Fiche</th>
                                                                <th>Autre Saisie</th>
                                                                <th>Autre Modif</th>
                                                                <th>ReadOnly</th>
                                                                <th>Valeur par d�faut</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="content_autre"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>             
                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-next btn-fill btn-rose btn-wd' name='next' value='Suivant' />
                                        <input type='button' class='btn btn-finish btn-fill btn-rose btn-wd' id="submitform" name='finish' value='G�n�rer' />
                                    </div>
                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Pr�c�dent' />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                        </form>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $().ready(function(){
        var $validator = $('.wizard-card form').validate({
            rules: {},

            errorPlacement: function(error, element) {
                $(element).parent('div').addClass('has-error');
            }
    	});

        // Wizard Initialization
      	$('.wizard-card').bootstrapWizard({
            'tabClass': 'nav nav-pills',
            'nextSelector': '.btn-next',
            'previousSelector': '.btn-previous',

            onNext: function(tab, navigation, index) {
            	var $valid = $('.wizard-card form').valid();
            	if(!$valid) {
            		$validator.focusInvalid();
            		return false;
            	}
                
            },

            onInit : function(tab, navigation, index){

              //check number of tabs and fill the entire row
              var $total = navigation.find('li').length;
              $width = 100/$total;
              var $wizard = navigation.closest('.wizard-card');

              $display_width = $(document).width();

              if($display_width < 600 && $total > 3){
                  $width = 50;
              }

               navigation.find('li').css('width',$width + '%');
               $first_li = navigation.find('li:first-child a').html();
               $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
               $('.wizard-card .wizard-navigation').append($moving_div);
               refreshAnimation($wizard, index);
               $('.moving-tab').css('transition','transform 0s');
           },

            onTabClick : function(tab, navigation, index){
                var $valid = $('.wizard-card form').valid();

                if(!$valid){
                    return false;
                } else{
                    return true;
                }
            },

            onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;

                var $wizard = navigation.closest('.wizard-card');

                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $($wizard).find('.btn-next').hide();
                    $($wizard).find('.btn-finish').show();
                } else {
                    $($wizard).find('.btn-next').show();
                    $($wizard).find('.btn-finish').hide();
                }

                button_text = navigation.find('li:nth-child(' + $current + ') a').html();

                setTimeout(function(){
                    $('.moving-tab').text(button_text);
                }, 150);

                var checkbox = $('.footer-checkbox');

                if( !index == 0 ){
                    $(checkbox).css({
                        'opacity':'0',
                        'visibility':'hidden'
                    });
                } else {
                    $(checkbox).css({
                        'opacity':'1',
                        'visibility':'visible',
                        'position':'absolute'
                    });
                }                        

                
                
                if(index==0){
                    
                }
                else if(index==1){
                    console.log('Preparing title and prefixe');
                    //Remplir file name et file title
                    //check selected amin'ny radio table
                    //disable anti_view
                    var nomTable = $('#source_table_text').val();
                    
                    var default_prefixe = nomTable.toLowerCase();
                    var default_title   = nomTable.toLowerCase();
                    var default_table   = nomTable.toUpperCase();
                    
                    
                    var tab_apj=['Liste','Analyse','Fiche','Saisie','Modification'];
                    var tab_apj_min =['liste','analyse','fiche','saisie','modif'];
                    
                    var file_title              =$('#file_title');
                    var file_title_liste        =$('#file_title_liste');
                    var file_title_analyse      =$('#file_title_analyse');
                    var file_title_fiche        =$('#file_title_fiche');
                    var file_title_saisie       =$('#file_title_saisie');
                    var file_title_modif        =$('#file_title_modif');
                    
                    var file_titles =[
                        file_title_liste,
                        file_title_analyse,
                        file_title_fiche,
                        file_title_saisie,
                        file_title_modif
                    ];
       
                    var file_prefixe            =$('#file_prefixe');
                    var file_prefixe_liste      =$('#file_prefixe_liste');
                    var file_prefixe_analyse    =$('#file_prefixe_analyse');
                    var file_prefixe_fiche      =$('#file_prefixe_fiche');
                    var file_prefixe_saisie     =$('#file_prefixe_saisie');
                    var file_prefixe_modif      =$('#file_prefixe_modif');
                    
                    var file_prefixes =[
                        file_prefixe_liste,
                        file_prefixe_analyse,
                        file_prefixe_fiche,
                        file_prefixe_saisie,
                        file_prefixe_modif
                    ];
                    
                    var table_name            =$('#table_name');
                    var table_name_liste      =$('#table_name_liste');
                    var table_name_analyse    =$('#table_name_analyse');
                    var table_name_fiche      =$('#table_name_fiche');
                    var table_name_saisie     =$('#table_name_saisie');
                    var table_name_modif      =$('#table_name_modif');
                    
                    var table_names =[
                        table_name_liste,
                        table_name_analyse,
                        table_name_fiche,
                        table_name_saisie,
                        table_name_modif
                    ];
                    
                    file_title.val(setDefault(file_title.val()      ,default_title));
                    file_prefixe.val(setDefault(file_prefixe.val()  ,default_prefixe));
                    table_name.val(setDefault(table_name.val()      ,default_table));
                    
                    for(var i=0;i<file_titles.length;i++){
                        file_titles[i].val( setDefault( file_titles[i].val(),tab_apj[i]+" "+file_title.val() ) );                     
                    }                        
                    
                    for(var i=0;i<file_prefixes.length;i++){
                        file_prefixes[i].val(setDefault(file_prefixes[i].val(),file_prefixe.val()+"-"+tab_apj_min[i] ));     
                    }
                    
                    for(var i=0;i<table_names.length;i++){
                        table_names[i].val(setDefault(table_names[i].val(),table_name.val() ));     
                    }
                    
                    var l_base = $('#apj_file_type_liste');
                    var a_base = $('#apj_file_type_analyse');
                    var f_base = $('#apj_file_type_fiche');
                    var s_base = $('#apj_file_type_saisie');
                    var m_base = $('#apj_file_type_modif');
                    
                    if(!l_base.prop('checked')){
                        $('.l_base').fadeOut();
                    }
                    else{
                        $('.l_base').fadeIn();
                    }

                    if(!a_base.prop('checked')){
                        $('.a_base').fadeOut();
                    }
                    else{
                        $('.a_base').fadeIn();
                    }

                    if(!f_base.prop('checked')){
                        $('.f_base').fadeOut();
                    }
                    else{
                        $('.f_base').fadeIn();
                    }

                    if(!s_base.prop('checked')){
                        $('.s_base').fadeOut();
                    }
                    else{
                        $('.s_base').fadeIn();
                    }

                    if(!m_base.prop('checked')){
                        $('.m_base').fadeOut();
                    }
                    else{
                        $('.m_base').fadeIn();
                    }
                        
                }
                else if(index==2){                    
                    var champ_criteria  =$('#source_criteria').val();
                    var liste_attributs =champ_criteria.split(",");                    
                    var contenu_champ_body ='';
                    
                    var champ_body_content= $('#champ_body').html().trim();                                  
                    if(champ_body_content=='' || champ_body_content==null || champ_body_content==undefined){
                        console.log(liste_attributs.length+'\n attributs d�tect�s');                     
                        for(var i=0;i<liste_attributs.length;i++){
                            var default_lien='';
                            var default_lien_url='';
                            var default_checked_first=' checked="true" ';
                            if(i==0){
                                default_lien=' checked="true" ';
                                default_lien_url=$('#file_prefixe_fiche').val()+'.jsp';
                                default_checked_first = ' disabled ';
                            }
                            contenu_champ_body
                                        +='<tr>'
                                            +'\n<td>'+liste_attributs[i]+'\n</td>'    
                                    
                                    
                                            +'\n<td>' 
                                                +'\n<div class="row">'
                                                    +'\n<div class="l_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" id="l_affiche_'+i+'" name="l_affiche_'+i+'" checked data-current="'+i+'" >L'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                    +'\n<div class="a_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" id="a_affiche_'+i+'" name="a_affiche_'+i+'" checked data-current="'+i+'" >A'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                    +'\n<div class="f_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" id="f_affiche_'+i+'" name="f_affiche_'+i+'" checked data-current="'+i+'" >F'
                                                        +'\n</label>'
                                                    +'\n</div>'     
                                                    +'\n<div class="s_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" id="s_affiche_'+i+'" name="s_affiche_'+i+'" '+default_checked_first+' data-current="'+i+'" >S'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                    +'\n<div class="m_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" id="m_affiche_'+i+'" name="m_affiche_'+i+'" '+default_checked_first+' data-current="'+i+'" >M'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                                +'\n<div class="row">'
                                                    +'\n<div class="col-md-11">'
                                                        +'\n<div class="form-group label-floating">'
                                                           +'\n<label class="control-label">Libell�'
                                                                +'\n<small></small>'
                                                            +'\n</label>'
                                                            +'\n<input name="input_affiche_'+i+'" id="input_affiche_'+i+'" value="'+liste_attributs[i]+'" type="text" class="form-control input_affiche" data-current="'+i+'" >'
                                                        +'\n</div>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                            +'\n</td>'
                                    
                                    
                                            +'\n<td>' 
                                                    +'\n<div class="row">'
                                                        +'\n<div class="l_base checkbox-inline">'
                                                            +'\n<label>'
                                                                +'\n<input type="checkbox" name="l_lien_'+i+'" id="l_lien_'+i+'" '+default_lien+' data-current="'+i+'" >L'
                                                            +'\n</label>'
                                                            +'\n</div>'
                                                        +'\n<div class="a_base checkbox-inline">'
                                                            +'\n<label>'
                                                                +'\n<input type="checkbox" name="a_lien_'+i+'" id="a_lien_'+i+'" '+default_lien+' data-current="'+i+'" >A'
                                                            +'\n</label>'
                                                        +'\n</div>'
                                                        +'\n<div class="f_base checkbox-inline">'
                                                            +'\n<label>'
                                                                +'\n<input type="checkbox" name="f_lien_'+i+'" id="f_lien_'+i+'" data-current="'+i+'" >F'
                                                            +'\n</label>'
                                                        +'\n</div>'    
                                                    +'\n</div>'
                                                    +'\n<div class="row">'
                                                        +'\n<div class="col-md-11">'
                                                            +'\n<div class="form-group label-floating">'
                                                                +'\n<label class="control-label">URL depuis pages/'
                                                                    +'\n<small></small>'
                                                                +'\n</label>'
                                                                +'\n<input name="input_lien_'+i+'" id="input_lien_'+i+'" value="'+default_lien_url+'" type="text" class="form-control" data-current="'+i+'" >'
                                                            +'\n</div>'
                                                        +'\n</div>'
                                                    +'\n</div>'
                                            +'\n</td>'
                                    
                                    
                                            +'\n<td>' 
                                                +'\n<div class="row">'
                                                    +'\n<div class="l_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="l_somme_'+i+'" id="l_somme_'+i+'" data-current="'+i+'" >L'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                    +'\n<div class="a_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="a_somme_'+i+'" id="a_somme_'+i+'" data-current="'+i+'" >A'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                                +'\n<div class="row">'
                                                    +'\n<div class="col-md-1">'
                                                        +'\n<div class="form-group label-floating">'
                                                            +'\n<label class="control-label">'
                                                                +'\n<small></small>'
                                                            +'\n</label>'
                                                            +'\n<input name="input_somme_'+i+'" id="input_somme_'+i+'" type="text" class="form-control" disabled data-current="'+i+'" >'
                                                        +'\n</div>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                            +'\n</td>'
                                    
                                    
                                            +'\n<td>' 
                                                +'\n<div class="row">'
                                                    +'\n<div class="l_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="l_filtrer_'+i+'" id="l_filtrer_'+i+'" checked="true" data-current="'+i+'" >L'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                    +'\n<div class="a_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="a_filtrer_'+i+'" id="a_filtrer_'+i+'" checked="true" data-current="'+i+'" >A'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                                +'\n<div class="row">'
                                                    +'\n<div class="col-md-11">'
                                                        +'\n<div class="form-group label-floating">'
                                                            +'\n<label class="control-label">Libell� filtre'
                                                                +'\n<small></small>'
                                                            +'\n</label>'
                                                            +'\n<input name="input_filtrer_'+i+'" id="input_filtrer_'+i+'" value="'+liste_attributs[i]+'" type="text" class="form-control input_affiche_child" data-current="'+i+'" >'
                                                        +'\n</div>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                            +'\n</td>'
                                    
                                                                        
                                            +'\n<td>' 
                                                +'\n<div class="row">'
                                                    +'\n<div class="l_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="l_tableau_'+i+'" id="l_tableau_'+i+'" checked="true" data-current="'+i+'" >L'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                    +'\n<div class="a_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="a_tableau_'+i+'" id="a_tableau_'+i+'" checked="true" data-current="'+i+'" >A'
                                                        +'\n</label>'
                                                    +'\n</div>'                                                       
                                                +'\n</div>'
                                                +'\n<div class="row">'
                                                    +'\n<div class="col-md-11">'
                                                        +'\n<div class="form-group label-floating">'
                                                            +'\n<label class="control-label">Libell� tableau'
                                                                +'\n<small></small>'
                                                            +'\n</label>'
                                                            +'\n<input name="input_tableau_'+i+'" id="input_tableau_'+i+'" value="'+liste_attributs[i]+'" type="text" class="form-control input_affiche_child" data-current="'+i+'" >'
                                                        +'\n</div>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                            +'\n</td>'
                                                                        
                                            +'\n<td>' 
                                                +'\n<div class="row">'
                                                    +'\n<div class="l_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="l_intervalle_'+i+'" id="l_intervalle_'+i+'" data-current="'+i+'" >L'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                    +'\n<div class="a_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="a_intervalle_'+i+'" id="a_intervalle_'+i+'" data-current="'+i+'" >A'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                                +'\n<div class="row">'
                                                    +'\n<div class="col-md-11">'
                                                        +'\n<div class="form-group label-floating">'
                                                            +'\n<label class="control-label">Suffixe "min;max"'
                                                                +'\n<small></small>'
                                                            +'\n</label>'
                                                            +'\n<input name="input_intervalle_'+i+'" id="input_intervalle_'+i+'" type="text" class="form-control" data-current="'+i+'" >'
                                                        +'\n</div>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                            +'\n</td>'
                                                                        
                                            +'\n<td>' 
                                                +'\n<div class="row">'
                                                    +'\n<div class="l_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="l_typeobjet_'+i+'" id="l_typeobjet_'+i+'" data-current="'+i+'" >L'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                    +'\n<div class="a_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="a_typeobjet_'+i+'" id="a_typeobjet_'+i+'" data-current="'+i+'" >A'
                                                        +'\n</label>'
                                                    +'\n</div>'

                                                    +'\n<div class="s_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="s_typeobjet_'+i+'" id="s_typeobjet_'+i+'" data-current="'+i+'" >S'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                    +'\n<div class="m_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="m_typeobjet_'+i+'" id="m_typeobjet_'+i+'" data-current="'+i+'" >M'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                                +'\n<div class="row">'
                                                    +'\n<div class="col-md-11">'
                                                        +'\n<div class="form-group label-floating">'
                                                            +'\n<label class="control-label">Config source'
                                                                +'\n<small></small>'
                                                            +'\n</label>'
                                                            +'\n<input name="input_typeobjet_'+i+'" id="input_typeobjet_'+i+'" type="text" class="form-control" data-current="'+i+'" >'
                                                        +'\n</div>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                            +'\n</td>'
                                                                       
                                            +'\n<td>' 
                                                +'\n<div class="row">'                                                 
                                                    +'\n<div class="s_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="s_popup_'+i+'" id="s_popup_'+i+'" data-current="'+i+'" >S'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                    +'\n<div class="m_base checkbox-inline">'
                                                        +'\n<label>'
                                                            +'\n<input type="checkbox" name="m_popup_'+i+'" id="m_popup_'+i+'" data-current="'+i+'" >M'
                                                        +'\n</label>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                                +'\n<div class="row">'
                                                    +'\n<div class="col-md-11">'
                                                        +'\n<div class="form-group label-floating">'
                                                            +'\n<label class="control-label">Source de pop-up pages/'
                                                                +'\n<small></small>'
                                                            +'\n</label>'
                                                            +'\n<input name="input_popup_'+i+'" id="input_popup_'+i+'" type="text" class="form-control" data-current="'+i+'" >'
                                                        +'\n</div>'
                                                    +'\n</div>'
                                                +'\n</div>'
                                            +'\n</td>'
                                        +'\n</tr>';                                                               
                                
                        } 

                        
                        
                    }
                    
                    $('#champ_body').html(contenu_champ_body);
                        
                    var l_base = $('#apj_file_type_liste').prop('checked');
                    var a_base = $('#apj_file_type_analyse').prop('checked');
                    var f_base = $('#apj_file_type_fiche').prop('checked');
                    var s_base = $('#apj_file_type_saisie').prop('checked');
                    var m_base = $('#apj_file_type_modif').prop('checked');

                    if(!l_base){
                        $('.l_base').fadeOut();
                    }
                    else{
                        $('.l_base').fadeIn();
                    }

                    if(!a_base){
                        $('.a_base').fadeOut();
                    }
                    else{
                        $('.a_base').fadeIn();
                    }

                    if(!f_base){
                        $('.f_base').fadeOut();
                    }
                    else{
                        $('.f_base').fadeIn();
                    }

                    if(!s_base){
                        $('.s_base').fadeOut();
                    }
                    else{
                        $('.s_base').fadeIn();
                    }

                    if(!m_base){
                        $('.m_base').fadeOut();
                    }
                    else{
                        $('.m_base').fadeIn();
                    }
                    

                    /*for(var i=0;i<liste_attributs.length;i++){                    
                        $('#input_affiche_'+i).keyup(function(event){
                            var current_val = $(this).val();  
                            console.log('current_val'+current_val);
                            console.log('#input_filtrer_'+i);
                            console.log($('#input_filtrer_'+i));
                            console.log($('#input_filtrer_'+i).val());
                            $('#input_filtrer_'+i).val(''+current_val);
                            $('#input_tableau_'+i).val(''+current_val);
                            console.log($('#input_filtrer_'+i).val());
                        });
                    }*/
                }
                else if(index==3){
                    var champ_criteria  =$('#source_criteria').val();
                    var liste_attributs =champ_criteria.split(",");                    
                    var contenu_champ_autre ='';
                    
                    var champ_body_content= $('#content_autre').html().trim();                                  
                    if(champ_body_content=='' || champ_body_content==null || champ_body_content==undefined){                                          
                        for(var i=0;i<liste_attributs.length;i++){
                            contenu_champ_autre+='<tr>'
                                                    +'\n<td>'+liste_attributs[i]+'</td>'
                                                    +'\n<td>'
                                                        +'\n<div class="row">'
                                                            +'\n<div class="col-md-11">'
                                                                +'\n<div class="form-group label-floating">'
                                                                    +'\n<label class="control-label">Autre liste'
                                                                        +'\n<small></small>'
                                                                    +'\n</label>'
                                                                    +'\n<input name="l_input_autre_'+i+'" id="l_input_autre_'+i+'" type="text" class="form-control" data-current="'+i+'" >'
                                                                +'\n</div>'
                                                            +'\n</div>'
                                                        +'\n</div>'
                                                    +'\n</td>'
                                                    +'\n<td>'
                                                        +'\n<div class="row">'
                                                            +'\n<div class="col-md-11">'
                                                                +'\n<div class="form-group label-floating">'
                                                                    +'\n<label class="control-label">Autre Liste'
                                                                        +'\n<small></small>'
                                                                    +'\n</label>'
                                                                    +'\n<input name="a_input_autre_'+i+'" id="a_input_autre_'+i+'" type="text" class="form-control" data-current="'+i+'" >'
                                                                +'\n</div>'
                                                            +'\n</div>'
                                                        +'\n</div>'
                                                    +'\n</td>'
                                                    +'\n<td>'
                                                        +'\n<div class="row">'
                                                            +'\n<div class="col-md-11">'
                                                                +'\n<div class="form-group label-floating">'
                                                                    +'\n<label class="control-label">Autre Fiche'
                                                                        +'\n<small></small>'
                                                                    +'\n</label>'
                                                                    +'\n<input name="f_input_autre_'+i+'" id="f_input_autre_'+i+'" type="text" class="form-control" data-current="'+i+'" >'
                                                                +'\n</div>'
                                                            +'\n</div>'
                                                        +'\n</div>'
                                                    +'\n</td>'
                                                    +'\n<td>'
                                                        +'\n<div class="row">'
                                                            +'\n<div class="col-md-11">'
                                                                +'\n<div class="form-group label-floating">'
                                                                    +'\n<label class="control-label">Autre Saisie'
                                                                        +'\n<small></small>'
                                                                    +'\n</label>'
                                                                    +'\n<input name="s_input_autre_'+i+'" id="s_input_autre_'+i+'" type="text" class="form-control" data-current="'+i+'" >'
                                                                +'\n</div>'
                                                            +'\n</div>'
                                                        +'\n</div>'
                                                    +'\n</td>'
                                                    +'\n<td>'
                                                        +'\n<div class="row">'
                                                            +'\n<div class="col-md-11">'
                                                                +'\n<div class="form-group label-floating">'
                                                                    +'\n<label class="control-label">Autre Modif'
                                                                        +'\n<small></small>'
                                                                    +'\n</label>'
                                                                    +'\n<input name="m_input_autre_'+i+'" id="m_input_autre_'+i+'" type="text" class="form-control" data-current="'+i+'" >'
                                                                +'\n</div>'
                                                            +'\n</div>'
                                                        +'\n</div>'
                                                    +'\n</td>'
                                                    +'\n<td>'
                                                        +'\n<div class="s_base checkbox-inline">'
                                                            +'\n<label>'
                                                                +'\n<input type="checkbox" name="s_readonly_'+i+'" id="s_readonly_'+i+'" data-current="'+i+'" >S'
                                                            +'\n</label>'
                                                        +'\n</div>'
                                                        +'\n<div class="m_base checkbox-inline">'
                                                            +'\n<label>'
                                                                +'\n<input type="checkbox" name="m_readonly_'+i+'" id="m_readonly_'+i+'" data-current="'+i+'" >M'
                                                            +'\n</label>'
                                                        +'\n</div>'   
                                                    +'\n</td>'
                                                    +'\n<td>'
                                                        
                                                        +'\n<div class="row">'
                                                            +'\n<div class="col-md-11">'
                                                                +'\n<div class="form-group label-floating">'
                                                                    +'\n<label class="control-label">Valeur par d�faut'
                                                                        +'\n<small></small>'
                                                                    +'\n</label>'
                                                                    +'\n<input name="input_default_'+i+'" id="input_default_'+i+'" type="text" class="form-control" data-current="'+i+'" >'
                                                                +'\n</div>'
                                                            +'\n</div>'
                                                        +'\n</div>'
                                                    +'\n</td>'
                                                +'\n</tr>';
                        }
                        $('#content_autre').html(contenu_champ_autre);
                    }
                    
                    
                }
                
                refreshAnimation($wizard, index);
            }
      	});


        // Prepare the preview for profile picture
        $("#wizard-picture").change(function(){
            readURL(this);
        });

        $('[data-toggle="wizard-radio"]').click(function(){
            wizard = $(this).closest('.wizard-card');
            wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
            $(this).addClass('active');
            $(wizard).find('[type="radio"]').removeAttr('checked');
            $(this).find('[type="radio"]').attr('checked','true');
        });

        $('[data-toggle="wizard-checkbox"]').click(function(){
            
            if( $(this).hasClass('active')){
                $(this).removeClass('active');
                $(this).find('[type="checkbox"]').removeAttr('checked');
                
            } else {
                $(this).addClass('active');
                $(this).find('[type="checkbox"]').attr('checked','true');
            }
            
            var l_base = $('#apj_file_type_liste').prop('checked');
            var a_base = $('#apj_file_type_analyse').prop('checked');
            var f_base = $('#apj_file_type_fiche').prop('checked');
            var s_base = $('#apj_file_type_saisie').prop('checked');
            var m_base = $('#apj_file_type_modif').prop('checked');

            if(!l_base){
                $('.l_base').fadeOut();
            }
            else{
                $('.l_base').fadeIn();
            }

            if(!a_base){
                $('.a_base').fadeOut();
            }
            else{
                $('.a_base').fadeIn();
            }

            if(!f_base){
                $('.f_base').fadeOut();
            }
            else{
                $('.f_base').fadeIn();
            }

            if(!s_base){
                $('.s_base').fadeOut();
            }
            else{
                $('.s_base').fadeIn();
            }

            if(!m_base){
                $('.m_base').fadeOut();
            }
            else{
                $('.m_base').fadeIn();
            }
        });

        $('.set-full-height').css('height', 'auto');

         //Function to show image before upload

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(window).resize(function(){
            $('.wizard-card').each(function(){
                $wizard = $(this);
                index = $wizard.bootstrapWizard('currentIndex');
                refreshAnimation($wizard, index);

                $('.moving-tab').css({
                    'transition': 'transform 0s'
                });
            });
        });

        function refreshAnimation($wizard, index){
            total_steps = $wizard.find('li').length;
            move_distance = $wizard.width() / total_steps;
            step_width = move_distance;
            move_distance *= index;

            $current = index + 1;

            if($current == 1){
                move_distance -= 8;
            } else if($current == total_steps){
                move_distance += 8;
            }

            $wizard.find('.moving-tab').css('width', step_width);
            $('.moving-tab').css({
                'transform':'translate3d(' + move_distance + 'px, 0, 0)',
                'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
            });
        }
        
        
        $('#newmap_submit').click(function(event){
            event.stopPropagation();
            //Alaina daholo ny valeur rehetra
            var newmap_database     =$('#newmap_database').val();
            var newmap_mapping      =$('#newmap_mapping').val();
            var newmap_script       =$('#newmap_script').val();
            var newmap_function     =$('#newmap_function').prop('checked');
            var newmap_sequence     =$('#newmap_sequence').prop('checked');
            var newmap_parent       =$('#newmap_parent').val();
            var newmap_destination  =$('#newmap_destination').val();            
            
            //Ampiana valeur hidden
            //Alefa any amin'ny page iray mandray anazy
            
            $.ajax({
                type: 'POST',
                url: '<%=url_map%>',
                dataType:'text',
                data:{
                    map:'true',
                    newmap_database:newmap_database,
                    newmap_mapping:newmap_mapping,
                    newmap_script:newmap_script,
                    newmap_function:newmap_function,
                    newmap_sequence:newmap_sequence,
                    newmap_parent:newmap_parent,
                    newmap_destination:newmap_destination
                },
                success: function(response){
                    console.log(response);
                    
                    if(response==='error'){
                        swal(
                            'APJ',
                            'Erreur lors du mapping',
                            'error'
                        );
                    }
                   
                    //response = JSON.parse(JSON.stringify(response));                   
                    
                    //console.log(response);
                    
                    var nomTable    =response.split(',')[0];
                    var nomPackage  =response.split(',')[1];
                    var listeCritere=response.split(",")[2];
                    
                    console.log('nomTable=='+nomTable);
                    console.log('nomPackage=='+nomPackage);
                    console.log('listeCritere=='+listeCritere);
                    listeCritere=listeCritere.replace(/;/g,",");
                    //alert('Mappage de la classe '+nomPackage+'\n effectu� avec succ�s!');
                    
                    swal(
                        'APJ',
                        'Mappage de la classe '+nomPackage+'\n effectu� avec succ�s!',
                        'success'
                    );
                    
                    $('#source_table_text').val(nomTable);
                    $('#source_classe').val(nomPackage);
                    $('#source_criteria').val(listeCritere);
                },
                error:function (jqXHR,textStatus,errorThrown ){
                    console.log('TextStatus=='+textStatus);
                    console.log(errorThrown);
                    console.log('Erreur creation fichier');
                }
            });
            
            //Mi-executer fonction mi-cr�er fichier
            return false;
        });
        
        $('#source_table_text').keyup(function () {
           var data_source=[''];
           var table_source = <%=table_source%>
           var view_source = <%=view_source%>
           //V�rifier radio si table alors liste table
           var source_table_radio = $('#soure_table_radio_parent').find('[type=radio]');           
           for(var i=0;i<source_table_radio.length;i++){               
               if( $('#soure_table_radio_parent').find('[type=radio]')[i].checked){                
                   $('#source_table_radio').val(i);
               }
           }
           
           //console.log(source_table_radio);
           //$('#source_table_radio').val(source_table_radio);
           
           //Si vue alors liste vue
           switch(parseInt($('#source_table_radio').val())){
               case 0:data_source=table_source;break;
               case 1:data_source=view_source;break;
               case 2:data_source=[''];break;
               default:data_source=[''];break;
           }
           
           //Si nouvelle table alors vide
           $("#source_table_text").autocomplete({
                source: data_source
           });
        });
                
        var tab_apj=['Liste','Analyse','Fiche','Saisie','Modification'];
        var tab_apj_min =['liste','analyse','fiche','saisie','modif'];
        
        $('#file_title').keyup(function () {
            var file_title              =$('#file_title');
            var file_title_liste        =$('#file_title_liste');
            var file_title_analyse      =$('#file_title_analyse');
            var file_title_fiche        =$('#file_title_fiche');
            var file_title_saisie       =$('#file_title_saisie');
            var file_title_modif        =$('#file_title_modif');

            var file_titles =[
                file_title_liste,
                file_title_analyse,
                file_title_fiche,
                file_title_saisie,
                file_title_modif
            ];
            
            for(var i=0;i<file_titles.length;i++){        
                if(file_title.val().trim()==='')
                    file_titles[i].val('');
                file_titles[i].val( tab_apj[i]+" "+file_title.val() );                
            }
                
        });
        
        $('#file_prefixe').keyup(function(){
            var file_prefixe            =$('#file_prefixe');
            var file_prefixe_liste      =$('#file_prefixe_liste');
            var file_prefixe_analyse    =$('#file_prefixe_analyse');
            var file_prefixe_fiche      =$('#file_prefixe_fiche');
            var file_prefixe_saisie     =$('#file_prefixe_saisie');
            var file_prefixe_modif      =$('#file_prefixe_modif');

            var file_prefixes =[
                file_prefixe_liste,
                file_prefixe_analyse,
                file_prefixe_fiche,
                file_prefixe_saisie,
                file_prefixe_modif
            ];            
            
            for(var i=0;i<file_prefixes.length;i++){                   
                if(file_prefixe.val().trim()==='')
                    file_prefixes[i].val('');
                file_prefixes[i].val( file_prefixe.val()+"-"+tab_apj_min[i] );              
            }
        });
        
         $('#table_name').keyup(function(){
            var table_name            =$('#table_name');
            var table_name_liste      =$('#table_name_liste');
            var table_name_analyse    =$('#table_name_analyse');
            var table_name_fiche      =$('#table_name_fiche');
            var table_name_saisie     =$('#table_name_saisie');
            var table_name_modif      =$('#table_name_modif');

            var table_names =[
                table_name_liste,
                table_name_analyse,
                table_name_fiche,
                table_name_saisie,
                table_name_modif
            ];            
            
            for(var i=0;i<table_names.length;i++){                   
                if(table_name.val().trim()==='')
                    table_names[i].val('');
                table_names[i].val( table_name.val());              
            }
        });
        
        $('#simple_advanced').click(function(){
            if($('#simple_advanced').prop('checked'))
                $('.simple_advanced').css('display', 'block');
            else $('.simple_advanced').css('display', 'none');
        });
        
        $('#submitform').click(function(){
            var to_send_url='<%=url%>';
            var to_send_destination = $('#newmap_destination').val();
            var to_send_mappage=$('#newmap_mapping').val();
            var to_send_source_classe=$('#source_classe').val();
            
            
            
            
            var liste_file      = [  $('#apj_file_type_liste').prop('checked')
                                    ,$('#apj_file_type_analyse').prop('checked')
                                    ,$('#apj_file_type_fiche').prop('checked')
                                    ,$('#apj_file_type_saisie').prop('checked')
                                    ,$('#apj_file_type_modif').prop('checked')
                                ];
                                
            var liste_file_name = [  $('#file_prefixe_liste').val()
                                    ,$('#file_prefixe_analyse').val()
                                    ,$('#file_prefixe_fiche').val()
                                    ,$('#file_prefixe_saisie').val()
                                    ,$('#file_prefixe_modif').val()
                                ];
                                
            var liste_file_title= [  $('#file_title_liste').val()
                                    ,$('#file_title_analyse').val()
                                    ,$('#file_title_fiche').val()
                                    ,$('#file_title_saisie').val()
                                    ,$('#file_title_modif').val()
                                ];
            var liste_table_name= [  $('#table_name_liste').val()
                                    ,$('#table_name_analyse').val()
                                    ,$('#table_name_fiche').val()
                                    ,$('#table_name_saisie').val()
                                    ,$('#table_name_modif').val()
                                ];

            var to_send_attributs = $('#source_criteria').val();            
            var liste_attributs = to_send_attributs.split(","); 
            
            var to_send_file        =stringArrayToString(liste_file);
            var to_send_file_name   =stringArrayToString(liste_file_name);
            var to_send_file_title  =stringArrayToString(liste_file_title);
            var to_send_table_name  =stringArrayToString(liste_table_name);
            
            var l_affiche_s = [];
            var a_affiche_s = [];
            var f_affiche_s = [];
            var s_affiche_s = [];
            var m_affiche_s = [];
            
            var l_lien_s    = [];
            var a_lien_s    = [];
            var f_lien_s    = [];
            
            var l_somme_s   = [];
            var a_somme_s   = [];
            
            var l_filtrer_s = [];
            var a_filtrer_s = [];
            
            var l_tableau_s = [];
            var a_tableau_s = [];
            
            var l_intervalle_s = [];
            var a_intervalle_s = [];
            
            var l_typeobjet_s = [];
            var a_typeobjet_s = [];
            var s_typeobjet_s = [];
            var m_typeobjet_s = [];
            
            var s_popup_s     = [];
            var m_popup_s     = [];
            
           
            var input_affiche_s     = [];
            var input_lien_s        = [];
            var input_filtrer_s     = [];
            var input_tableau_s     = [];
            var input_intervalle_s  = [];
            var input_typeobjet_s   = [];
            var input_popup_s       = [];
            
            var l_input_autre_s     = [];
            var a_input_autre_s     = [];
            var f_input_autre_s     = [];
            var s_input_autre_s     = [];
            var m_input_autre_s     = [];
            var s_readonly_s        = [];
            var m_readonly_s        = [];
            
            for(var k=0;k<liste_attributs.length;k++){                
                l_affiche_s         .push($('#l_affiche_'+k).prop('checked'));
                a_affiche_s         .push($('#a_affiche_'+k).prop('checked'));
                f_affiche_s         .push($('#f_affiche_'+k).prop('checked'));
                s_affiche_s         .push($('#s_affiche_'+k).prop('checked'));
                m_affiche_s         .push($('#m_affiche_'+k).prop('checked'));
                
                l_lien_s            .push($('#l_lien_'+k).prop('checked'));
                a_lien_s            .push($('#a_lien_'+k).prop('checked'));
                f_lien_s            .push($('#f_lien_'+k).prop('checked'));
                
                l_somme_s           .push($('#l_somme_'+k).prop('checked'));
                a_somme_s           .push($('#a_somme_'+k).prop('checked'));
                
                l_filtrer_s         .push($('#l_filtrer_'+k).prop('checked'));
                a_filtrer_s         .push($('#a_filtrer_'+k).prop('checked'));
                
                l_tableau_s         .push($('#l_tableau_'+k).prop('checked'));
                a_tableau_s         .push($('#a_tableau_'+k).prop('checked'));
                
                l_intervalle_s      .push($('#l_intervalle_'+k).prop('checked'));
                a_intervalle_s      .push($('#a_intervalle_'+k).prop('checked'));
                
                l_typeobjet_s       .push($('#l_typeobjet_'+k).prop('checked'));
                a_typeobjet_s       .push($('#a_typeobjet_'+k).prop('checked'));
                s_typeobjet_s       .push($('#s_typeobjet_'+k).prop('checked'));
                m_typeobjet_s       .push($('#m_typeobjet_'+k).prop('checked'));
                
                s_popup_s           .push($('#s_popup_'+k).prop('checked'));
                m_popup_s           .push($('#m_popup_'+k).prop('checked'));
                
                input_affiche_s     .push($('#input_affiche_'+k).val());
                input_lien_s        .push($('#input_lien_'+k).val());
                input_filtrer_s     .push($('#input_filtrer_'+k).val());
                input_tableau_s     .push($('#input_tableau_'+k).val());
                input_intervalle_s  .push($('#input_intervalle_'+k).val());
                input_typeobjet_s   .push($('#input_typeobjet_'+k).val());
                input_popup_s       .push($('#input_popup_'+k).val());
                
                l_input_autre_s     .push($('#l_input_autre_'+k).val());
                a_input_autre_s     .push($('#a_input_autre_'+k).val());
                f_input_autre_s     .push($('#f_input_autre_'+k).val());
                s_input_autre_s     .push($('#s_input_autre_'+k).val());
                m_input_autre_s     .push($('#m_input_autre_'+k).val());
                s_readonly_s        .push($('#s_readonly_'+k).prop('checked'));
                m_readonly_s        .push($('#m_readonly_'+k).prop('checked'));
            }
          
            var l_base = $('#apj_file_type_liste').prop('checked');
            var a_base = $('#apj_file_type_analyse').prop('checked');
            var f_base = $('#apj_file_type_fiche').prop('checked');
            var s_base = $('#apj_file_type_saisie').prop('checked');
            var m_base = $('#apj_file_type_modif').prop('checked');
            
            
            $.ajax({
                type: 'POST',
                url: to_send_url,
                data:{
                    generate_liste  :l_base,
                    generate_analyse:a_base,
                    generate_fiche  :f_base,
                    generate_saisie :s_base,
                    generate_modif  :m_base,
                    
                    map_file        :to_send_file,
                    map_file_name   :to_send_file_name,
                    map_file_title  :to_send_file_title,
                    map_table_name  :to_send_table_name,
                    map_destination :to_send_destination,
                    map_mappage     :to_send_mappage,
                    map_classe      :to_send_source_classe,
                    map_crit        :to_send_attributs,
                    
                    l_affiche_s     :stringArrayToString(l_affiche_s),
                    a_affiche_s     :stringArrayToString(a_affiche_s),
                    f_affiche_s     :stringArrayToString(f_affiche_s),
                    s_affiche_s     :stringArrayToString(s_affiche_s),
                    m_affiche_s     :stringArrayToString(m_affiche_s),
                    
                    l_lien_s        :stringArrayToString(l_lien_s),
                    a_lien_s        :stringArrayToString(a_lien_s),
                    f_lien_s        :stringArrayToString(f_lien_s),
                    
                    l_somme_s       :stringArrayToString(l_somme_s),
                    a_somme_s       :stringArrayToString(a_somme_s),
                    
                    l_filtrer_s     :stringArrayToString(l_filtrer_s),
                    a_filtrer_s     :stringArrayToString(a_filtrer_s),
                    
                    l_tableau_s     :stringArrayToString(l_tableau_s),
                    a_tableau_s     :stringArrayToString(a_tableau_s),
                    
                    l_intervalle_s  :stringArrayToString(l_intervalle_s),
                    a_intervalle_s  :stringArrayToString(a_intervalle_s),
                    
                    l_typeobjet_s   :stringArrayToString(l_typeobjet_s),
                    a_typeobjet_s   :stringArrayToString(a_typeobjet_s),
                    s_typeobjet_s   :stringArrayToString(s_typeobjet_s),
                    m_typeobjet_s   :stringArrayToString(m_typeobjet_s),
                    
                    s_popup_s       :stringArrayToString(s_popup_s),
                    m_popup_s       :stringArrayToString(m_popup_s),
                    
                    input_affiche_s     :stringArrayToString(input_affiche_s),
                    input_lien_s        :stringArrayToString(input_lien_s),
                    input_filtrer_s     :stringArrayToString(input_filtrer_s),
                    input_tableau_s     :stringArrayToString(input_tableau_s),
                    input_intervalle_s  :stringArrayToString(input_intervalle_s),
                    input_typeobjet_s   :stringArrayToString(input_typeobjet_s),
                    input_popup_s       :stringArrayToString(input_popup_s),

                    l_input_autre_s     :stringArrayToString(l_input_autre_s),
                    a_input_autre_s     :stringArrayToString(a_input_autre_s),
                    f_input_autre_s     :stringArrayToString(f_input_autre_s),
                    s_input_autre_s     :stringArrayToString(s_input_autre_s),
                    m_input_autre_s     :stringArrayToString(m_input_autre_s),
                    s_readonly_s        :stringArrayToString(s_readonly_s),
                    m_readonly_s        :stringArrayToString(m_readonly_s)
                    
                },
                success: function(response){
                    swal(
                        'APJ',
                        'Code sources g�n�r�es!',
                        'success'
                    );
                },
                error:function (jqXHR,textStatus,errorThrown ){
                    swal(
                        'APJ',
                        'Op�ration �chou�e! Veuillez r�essayer',
                        'error'
                    );
                }
            });
          
            console.log('Sending data');
        });
           
    });
        
    function map(){
        $.ajax({
            type: 'POST',
            url: '<%=url_map%>',
            data:{
                map:'true'
            },
            success: function(response){
                console.log('run');
            },
            error:function (jqXHR,textStatus,errorThrown ){
                console.log('Erreur run');
            }
        });
    }
    
    function setDefault(champ,defaultValue){
       return champ==undefined||champ==' '||champ==null?defaultValue:champ;
    }
    
    function changelibelle(i){
        console.log('valeur de '+i);
        var input_affiche = document.getElementById('input_affiche_'+i);
        var input_filtrer = document.getElementById('input_filtrer_'+i);
        var input_tableau = document.getElementById('input_tableau_'+i);
        input_filtrer.val(input_affiche.val());
        input_tableau.val(input_affiche.val());
        
        console.log('inputs_affiche'+input_affiche);
        console.log('inputs_filtrer'+input_filtrer);
        console.log('inputs_tableau'+input_tableau);
        
        console.log('Affiche.val='+input_affiche.val());
    }
    
    function stringArrayToString(array){
        var result = '';
           
        for(var i=0;i<array.length;i++)
            result+=','+array[i];
        
        return result.substring(1);
    }
</script>