<%@page import="Parameters.Template"%>
<%@page import="Parameters.Projet"%>
<!doctype html>
<html lang="en">

<head>
    <meta charset="windows-1252" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title><%=Projet.NAME_PROJECT%> || </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- CSS -->
    <jsp:include page='elements/css.jsp'/>
    <!-- Fin CSS -->
    <!-- JS -->
    <jsp:include page='elements/js.jsp'/>
    <!-- Fin JS -->
</head>
<body>
    

    <%

        String but = "asynctask/synchronisation.jsp";
        String lien = "module.jsp";
        String lienContenu = "index.jsp";
        String menu = "elements/menu/";
        String langue = "";
        if (request.getParameter("langue") != null) {
            session.setAttribute("langue", (String) request.getParameter("langue"));
        }
        langue = (String) session.getAttribute("langue");
    %>
    <div class="wrapper">
        <div class="sidebar" data-active-color="<%=Template.PROJECT_COLOR%>" data-background-color="black" <!--data-image="../../assets/img/sidebar-1.jpg"-->>
            <!-- Menu -->
            <jsp:include page='elements/menu/module.jsp'/>
            <!-- Fin Menu -->
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <!-- Header -->
                <jsp:include page='elements/header.jsp'/>
                <!-- Fin Header --> 
            </nav>
            <div class="content">
                <div class="container-fluid" style="margin-top: -70px;">
                    <!-- Contents -->
                    <% 
                    try {
                    %>
                        <jsp:include page='<%=but%>'/>
                    <%  
                    } 
                    catch (Exception e) {
                        e.printStackTrace();
                    %>
                        <script language="JavaScript"> 
                            swal('Erreur','<%=e.getMessage()%>','error');
                            history.back();
                        </script>
                    <%
                    }
                    %>
                    <!-- Fin Contents -->
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid" >
                    <!-- Footer -->
                    <jsp:include page='elements/footer.jsp'/>
                    <!-- Fin Footer -->
                </div>
            </footer>
        </div>
    </div>
</body>

</html>