<%@page import="periodictask.Synchronisation"%>
<%
    try {
        if (session.getAttribute("u") == null) {
%>
<script language="JavaScript">
    alert("Veuillez vous connecter pour acceder a ce contenu");
    document.location.replace("../index.jsp");
</script>
<%
        }
       
        if(request.getParameter("sync")!=null){
            String serverAddr           = request.getParameter("servAddr")          !=null?request.getParameter("servAddr")         :"";
            String serverPort           = request.getParameter("serverPort")        !=null?request.getParameter("serverPort")       :"";
            String serverType           = request.getParameter("serverType")        !=null?request.getParameter("serverType")       :"";
            String usernameSource       = request.getParameter("usernameSource")    !=null?request.getParameter("usernameSource")   :"";
            String pwdSource            = request.getParameter("pwdSource")         !=null?request.getParameter("pwdSource")        :"";
            String refreshtimesecond    = request.getParameter("refreshtimesecond") !=null?request.getParameter("refreshtimesecond"):"";
            String driver               = request.getParameter("driver")            !=null?request.getParameter("driver")           :"";
            
            int res = Integer.valueOf(request.getParameter("sync"));
            out.print("Valeur de res"+res);
            switch(res){
                case -1:    Synchronisation.current_synchro.clear()     ;out.print("CLEAR");break;
                case 1 :    Synchronisation.current_synchro.run()       ;out.print("RUN");break;
                case 2 :    Synchronisation.current_synchro.build(serverAddr,serverPort,serverType,usernameSource,pwdSource,refreshtimesecond,driver)   ;out.print("BUILD");break;
                default:    Synchronisation.current_synchro.stop()      ;out.print("STOP");break;
            }
        }    
%>
<%
    } catch (Exception e) {
        out.print("Erreur "+e.getMessage());
        e.printStackTrace();
        //out.println("<script language='JavaScript'> document.location.replace('erreur.jsp?message=" + e.getMessage() + "'); </script>");
        return;
    }
%>